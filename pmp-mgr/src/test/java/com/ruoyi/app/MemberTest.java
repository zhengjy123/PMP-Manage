package com.ruoyi.app;

import com.ruoyi.PMPManageApplication;
import com.ruoyi.system.domain.SysPost;
import com.ruoyi.system.mapper.SysPostMapper;
import com.ruoyi.web.app.projectmember.service.IPmpMemberService;
import com.ruoyi.web.app.projectmember.vo.PmpMemberVO;
import com.zjy.service.CustomService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Author: zjy
 * @Date: 2024/4/23 7:52
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PMPManageApplication.class)
@Slf4j
public class MemberTest {

    @Resource
    SysPostMapper sysPostMapper;

    // 自定义starter 测试
    @Resource
    CustomService customerService;

    /**
     * 自定义starter
     */
    @Test
    public void customStarterTest() {
        String hello = customerService.sayHello("zjy");
        log.debug("{}", hello);
    }

    @Test
    public void test() {
        long memberId = 1L;
        List<SysPost> list = sysPostMapper.selectPostsByMemberId(memberId);
        log.debug("list:{}", list);
    }

    @Resource
    IPmpMemberService iPmpMemberService;


    @Test
    public void test2() {

        PmpMemberVO vo =  iPmpMemberService.selectPmpMemberDetail(1L);
        log.info("vo {}",vo);
    }


}

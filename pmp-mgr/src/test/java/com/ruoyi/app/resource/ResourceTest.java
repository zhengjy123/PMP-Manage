package com.ruoyi.app.resource;

import com.ruoyi.PMPManageApplication;
import com.ruoyi.web.app.resource.human.domain.PmpResHuman;
import com.ruoyi.web.app.resource.human.mapper.PmpResHumanMapper;
import com.ruoyi.web.app.resource.human.pojo.PmpResHumanVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Author: zjy
 * @Date: 2024/4/23 7:52
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PMPManageApplication.class)
@Slf4j
public class ResourceTest {

    @Resource
    PmpResHumanMapper pmpResHumanMapper;

    /**
     * 自定义starter
     */
    @Test
    public void selectPmpResHumanProjectTest() {

        List<PmpResHumanVO> list = pmpResHumanMapper.selectPmpResHumanProject(new PmpResHuman());
        log.debug("vo:{}", list);
    }



}

package com.ruoyi.tech.threadpool;

import com.ruoyi.PMPManageApplication;
import com.ruoyi.web.test.NotificationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 线程池使用案例
 *
 * @author: zjy
 * 2024/4/26 0:33
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PMPManageApplication.class)
public class NotificationDemo {

    @Resource
    NotificationService notificationService;

    @Test
    public void test()  {

        // 待通知的用户
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");

        // 批量发送通知
        notificationService.sendNotificationsBatch(list);

    }

}

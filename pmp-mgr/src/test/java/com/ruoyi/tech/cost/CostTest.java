package com.ruoyi.tech.cost;

import com.ruoyi.PMPManageApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Pmp中的计算题
 * @Description:
 * @Author: zjy
 * @Date: 2024/4/8 17:23
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PMPManageApplication.class)
public class CostTest {

    private static final Logger log = LoggerFactory.getLogger(CostTest.class);

    /**
     * 挣值计算
     */
    @Test
    public void earnedValueCompute() {

        int ev = 10000;
        int pv = 12000;
        int ac = 8000;
        log.info("挣值 {} 计划值 ", ev, pv);
        log.info("成本偏差 {}", computeSV(ev, pv));
        log.info("进度偏差 {}", computeCV(ev, ac));
        log.info("cpi {} ", computeCPI(ev,ac));
        log.info("spi  {}", computeCPI(ev,pv));
    }

    /**
     * 成本偏差 = EV 挣值 - PV 计划值
     */
    public int computeSV(int ev,int pv){
        return ev - pv;
    }

    /**
     * 进度偏差 = EV 挣值 - AC 实际成本
     * @param ev 挣值
     * @param ac 实际成本
     * @return
     */
    public int computeCV(int ev,int ac){
        return ev - ac;
    }

    /**
     * 计算成本绩效指数
     * @return
     */
    public double computeCPI(int ev,int ac){
        return ev/ac;
    }

    /**
     * 计算进度绩效指数
     * @return
     */
    public double computeSPI(int ev,int pv){
        return ev/pv;
    }

}

package com.ruoyi.tech.activity;

import com.ruoyi.PMPManageApplication;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.Task;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Activity 6 教程
 * 视频：https://www.bilibili.com/video/BV1Fp4y19729?p=17&vd_source=6d981861200e8e4c9ef45dbffb0cfd11 声音不敢恭维
 *
 * @Description:
 * @Author: zjy
 * @Date: 2024/4/8 17:23
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PMPManageApplication.class)
public class ActivityTest {

    private static final Logger log = LoggerFactory.getLogger(ActivityTest.class);

    private ProcessEngine processEngine;
    private IdentityService identityService;
    private RepositoryService repositoryService;
    private RuntimeService runtimeService;
    private TaskService taskService;
    // 历史信息服务
    private HistoryService historyService;

    /**
     * P13 获取流程引擎
     *
     * P14 Activiti 7大服务介绍
     */
    @Before
    public void processEngineDemo() {

        ProcessEngineConfiguration processEngineConfiguration = ProcessEngineConfiguration.createStandaloneProcessEngineConfiguration();
        processEngineConfiguration.setJdbcDriver("com.mysql.cj.jdbc.Driver");
        processEngineConfiguration.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/activity_demo?useUnicode=true&characterEncoding=utf-8&nullCatalogMeansCurrent=true");
        processEngineConfiguration.setJdbcUsername("root");
        processEngineConfiguration.setJdbcPassword("1234");
        // 腾讯云
//        ProcessEngineConfiguration processEngineConfiguration = ProcessEngineConfiguration.createStandaloneProcessEngineConfiguration();
//        processEngineConfiguration.setJdbcDriver("com.mysql.cj.jdbc.Driver");
//        processEngineConfiguration.setJdbcUrl("jdbc:mysql://42.194.151.54:3306/activity_demo?useUnicode=true&characterEncoding=utf-8&nullCatalogMeansCurrent=true");
//        processEngineConfiguration.setJdbcUsername("root");
//        processEngineConfiguration.setJdbcPassword("129bfc6bd5e6cdd6");
        // 数据库策略 如果表不存在，自动创建表
        processEngineConfiguration.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);

        processEngine = processEngineConfiguration.buildProcessEngine();
        log.debug("processEngine {} ", processEngine);

        /**
         * Activiti 中每一个不同版本的业务流程的定义都需要使用一些定义文件，部署文件和支持数据
         * ( 例如 BPMN2.0 XML 文件，表单定义文件， 流程定义图像文件等 )，这些文件都存储在
         * Activiti 内建的 Repository 中。Repository Service 提供了对 repository 的存取服务。
         */
        repositoryService = processEngine.getRepositoryService();
        log.debug("repositoryService {} ", repositoryService);

        /**
         * 在 Activiti 中，每当一个流程定义被启动一次之后，都会生成一个相应的流程对象实例。
         * Runtime Service 提供了启动流程、查询流程实例、 设置获取流程实例变量等功能。此外
         * 它还提供了对流程部署，流程定义和流程实例的存取服务。
         */
        runtimeService = processEngine.getRuntimeService();

        /**
         * 在 Activiti 中业务流程定义中的每一个执行节点被称为一个 Task，对流程中的数据存取，
         * 状态变更等操作均需要在 Task 中完成。 Task Service 提供了对用户 Task 和 Form 相
         * 关的操作。它提供了运行时任务查询、领取、完成、删除以及变量设置等功能。
         */
        taskService = processEngine.getTaskService();

        /**
         * History Service 用于获取正在运行或已经完成的流程实例的信息，与 Runtime Service
         * 中获取的流程信息不同，历史信息包含已经持久化存储的永久信息，并已经被针对查询优化。
         */
        historyService = processEngine.getHistoryService();

        /**
         * Activiti 中内置了用户以及组管理的功能，必须使用这些用户和组的信息才能获取到相应的
         * Task。Identity Service 提供了对 Activiti 系统中的用户和组的管理功能。
         */
        identityService = processEngine.getIdentityService();

        /**
         *  Management Service 提供了对 Activiti 流程引擎的管理和维护功能，这些功能不在工作流驱动的应用程序中使用，主要用于 Activiti 系统的日常维护
         */
        ManagementService managementService = processEngine.getManagementService();

        log.debug("初始化完成！");
    }


    /**
     * P16 部署流程定义 part1
     */
    @Test
    public void createActivityUser(){

        // 创建用户
        createUser();

        // 创建用户组
        createUserGroup();

        // 建立关联关系
        createMembership();
    }

    /**
     * 创建用户
     */
    private void createUser(){
        // 初始化 act_id_user. axianlu . rensm
        User axianlu = identityService.newUser("U001");
        axianlu.setFirstName("一只闲鹿");
        identityService.saveUser(axianlu);

        User hr = identityService.newUser("U002");
        hr.setFirstName("人事喵");
        identityService.saveUser(hr);

        //assertEquals(2,identityService.createUserQuery().count());
    }

    /**
     * 创建用户组
     */
    private void createUserGroup(){

        Group deptLeader = identityService.newGroup("deptLeader");
        deptLeader.setName("管理层");
        deptLeader.setType("assignment");
        identityService.saveGroup(deptLeader);

        Group hr = identityService.newGroup("hr");
        hr.setName("人事部");
        hr.setType("assignment");
        identityService.saveGroup(hr);

        // 断言：检查通过createGroupQuery()方法返回的结果数量是否为2
        //assertEquals(2, identityService.createGroupQuery().count());

    }

    private void createMembership(){

        identityService.createMembership("U001","deptLeader");
        identityService.createMembership("U002","hr");
    }

    /**
     * P16 部署流程定义 part2
     * 数据保存在 act_re_deployment
     * act_ge_bytearray
     */
    @Test
    public void deployTest(){
        Deployment deployment = repositoryService.createDeployment().addClasspathResource("bpmn/leave.xml").deploy();
        log.debug("done!");
    }

    /**
     * P17 发起流程申请
     * act_hi_procinst 历史流程实例表
     * act_ru_task 任务列表
     */
    @Test
    public void submitApply(){

        String applyUserId = "chengxy2";
        // 设置启动流程的人员id
        identityService.setAuthenticatedUserId(applyUserId);
        // 开始流程，key是bpmn文件中定义的<process id="xxx"
        runtimeService.startProcessInstanceByKey("leave");

        //

    }

    /**
     * P18 获取待办
     * act_ru_task
     * P19 完成任务
     * act_ru_task
     * act_hi_procinst 关联
     */
    @Test
    public void queryTaskTodoList(){

        // P18

        // 根据当前id查询
        List<Task> list =
            taskService.createTaskQuery().processDefinitionKey("leave").taskAssignee("U001").active().list();
        log.debug("list {}",list);

        // 当前未签收任务
        List<Task> list2 =
                taskService.createTaskQuery().processDefinitionKey("leave").taskCandidateUser("U001").active().list();
        log.debug("list2 {}",list2);

        // P19 完成任务(部门领导审批)
        //
        Task task = list2.get(0);
        // 获取流程实例对象
        ProcessInstance processInstance
                = runtimeService.createProcessInstanceQuery().processDefinitionKey("leave").startedBy("chengxy2")
        .list().get(0);

        identityService.setAuthenticatedUserId("U001");
        // 审批意见
        taskService.addComment(task.getId(), processInstance.getId(), "【同意】准了");

        Map<String,Object> variables = new HashMap<>();
        // 设置bpmn中的流程变量
        variables.put("deptLeaderApproved", true);
        // 只有签收任务 act_hi_taskinst表的 assignee 才不会null
        taskService.claim(task.getId(),"U001");
        taskService.complete(task.getId(), variables);

    }

    /**
     * P20 我的已办
     * act_hi_taskinst
     */
    @Test
    public void taskDoneTest(){

        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery().processDefinitionKey("leave").taskAssignee("U001").finished().list();
        for(HistoricTaskInstance historicTaskInstance : list){
            String taskName = historicTaskInstance.getName();
            String assignee = historicTaskInstance.getAssignee();
            log.debug("历史任务名称 {} {} ",taskName,assignee);
        }
    }

    /**
     * P21 获取审批历史批注
     * act_hi_comment
     */
    @Test
    public void queryHistoryComment(){
        // 获取流程实例对象
        List<ProcessInstance> pList
                = runtimeService.createProcessInstanceQuery().processDefinitionKey("leave").startedBy("chengxy")
                .list();
        for (ProcessInstance processInstance : pList){
            // 这里的userTask表示用户活动，详见教程中 activity 常见活动类型
            // 从 act_hi_actinst 查询
            List<HistoricActivityInstance> hList  =
                    historyService.createHistoricActivityInstanceQuery().processInstanceId(processInstance.getId()).activityType("userTask").finished().list();
            //log.debug("list {} ",hList);
            for(HistoricActivityInstance historicActivityInstance : hList ){
                List<Comment> commentList = taskService.getTaskComments(historicActivityInstance.getTaskId(),"comment");
                for (Comment comment : commentList  ){
                    log.debug("历史批注 {}",comment.getFullMessage());
                }

            }
        }

    }

}

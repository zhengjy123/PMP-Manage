package com.ruoyi.web.app.quality.bug.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.web.app.quality.bug.domain.PmpQualityBug;
import com.ruoyi.web.app.quality.bug.mapper.PmpQualityBugMapper;
import com.ruoyi.web.app.quality.bug.service.IPmpQualityBugService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 缺陷管理Service业务层处理
 *
 * @author zjy
 * @date 2024-07-31
 */
@Service
public class PmpQualityBugServiceImpl implements IPmpQualityBugService
{
    @Resource
    private PmpQualityBugMapper pmpQualityBugMapper;

    /**
     * 查询缺陷管理
     *
     * @param id 缺陷管理ID
     * @return 缺陷管理
     */
    @Override
    public PmpQualityBug selectPmpQualityBugById(Long id)
    {
        return pmpQualityBugMapper.selectPmpQualityBugById(id);
    }

    /**
     * 查询缺陷管理列表
     *
     * @param pmpQualityBug 缺陷管理
     * @return 缺陷管理
     */
    @Override
    public List<PmpQualityBug> selectPmpQualityBugList(PmpQualityBug pmpQualityBug)
    {
        return pmpQualityBugMapper.selectPmpQualityBugList(pmpQualityBug);
    }

    /**
     * 新增缺陷管理
     *
     * @param pmpQualityBug 缺陷管理
     * @return 结果
     */
    @Override
    public int insertPmpQualityBug(PmpQualityBug pmpQualityBug)
    {
        pmpQualityBug.setCreateTime(DateUtils.getNowDate());
        return pmpQualityBugMapper.insertPmpQualityBug(pmpQualityBug);
    }

    /**
     * 修改缺陷管理
     *
     * @param pmpQualityBug 缺陷管理
     * @return 结果
     */
    @Override
    public int updatePmpQualityBug(PmpQualityBug pmpQualityBug)
    {
        return pmpQualityBugMapper.updatePmpQualityBug(pmpQualityBug);
    }

    /**
     * 删除缺陷管理对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmpQualityBugByIds(String ids)
    {
        return pmpQualityBugMapper.deletePmpQualityBugByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除缺陷管理信息
     *
     * @param id 缺陷管理ID
     * @return 结果
     */
    @Override
    public int deletePmpQualityBugById(Long id)
    {
        return pmpQualityBugMapper.deletePmpQualityBugById(id);
    }
}

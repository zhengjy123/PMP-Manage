package com.ruoyi.web.app.resource.human.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 资源管理 - 人力资源管理对象 pmp_res_human
 *
 * @author zjy
 * @date 2024-08-26
 */
@Data
public class PmpResHuman extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 计划天数 */
    @Excel(name = "计划天数")
    private Long planDay;

    /** 剩余天数 */
    @Excel(name = "剩余天数")
    private Long remainDay;

    /** 日花费成本 */
    @Excel(name = "日花费成本")
    private Double dailyCost;

    /** 资源类型 */
    @Excel(name = "资源类型")
    private Integer type;

    /** 项目id */
    private Long projectId;


}

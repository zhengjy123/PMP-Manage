package com.ruoyi.web.app.resource.human.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.web.app.resource.human.domain.PmpResHuman;
import com.ruoyi.web.app.resource.human.mapper.PmpResHumanMapper;
import com.ruoyi.web.app.resource.human.pojo.PmpResHumanVO;
import com.ruoyi.web.app.resource.human.service.IPmpResHumanService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 资源管理 - 人力资源管理Service业务层处理
 *
 * @author zjy
 * @date 2024-08-26
 */
@Service
public class PmpResHumanServiceImpl implements IPmpResHumanService
{
    @Resource
    private PmpResHumanMapper pmpResHumanMapper;

    /**
     * 查询资源管理 - 人力资源管理
     *
     * @param id 资源管理 - 人力资源管理ID
     * @return 资源管理 - 人力资源管理
     */
    @Override
    public PmpResHuman selectPmpResHumanById(Long id)
    {
        return pmpResHumanMapper.selectPmpResHumanById(id);
    }

    /**
     * 查询资源管理 - 人力资源管理列表
     *
     * @param pmpResHuman 资源管理 - 人力资源管理
     * @return 资源管理 - 人力资源管理
     */
    @Override
    public List<PmpResHuman> selectPmpResHumanList(PmpResHuman pmpResHuman)
    {
        return pmpResHumanMapper.selectPmpResHumanList(pmpResHuman);
    }

    /**
     * 新增资源管理 - 人力资源管理
     *
     * @param pmpResHuman 资源管理 - 人力资源管理
     * @return 结果
     */
    @Override
    public int insertPmpResHuman(PmpResHuman pmpResHuman)
    {
        pmpResHuman.setCreateTime(DateUtils.getNowDate());
        return pmpResHumanMapper.insertPmpResHuman(pmpResHuman);
    }

    /**
     * 修改资源管理 - 人力资源管理
     *
     * @param pmpResHuman 资源管理 - 人力资源管理
     * @return 结果
     */
    @Override
    public int updatePmpResHuman(PmpResHuman pmpResHuman)
    {
        return pmpResHumanMapper.updatePmpResHuman(pmpResHuman);
    }

    /**
     * 删除资源管理 - 人力资源管理对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmpResHumanByIds(String ids)
    {
        return pmpResHumanMapper.deletePmpResHumanByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除资源管理 - 人力资源管理信息
     *
     * @param id 资源管理 - 人力资源管理ID
     * @return 结果
     */
    @Override
    public int deletePmpResHumanById(Long id)
    {
        return pmpResHumanMapper.deletePmpResHumanById(id);
    }

    @Override
    public List<PmpResHumanVO> selectPmpResHumanProject(PmpResHuman pmpResHuman) {
        return pmpResHumanMapper.selectPmpResHumanProject(pmpResHuman);
    }
}

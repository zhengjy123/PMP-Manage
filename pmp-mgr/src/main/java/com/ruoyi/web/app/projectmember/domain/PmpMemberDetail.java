package com.ruoyi.web.app.projectmember.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 项目人员信息明细 pmp_detail
 *
 * @author zjy
 * @date 2024-04-19
 */
@Data
public class PmpMemberDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** memberId */
    private Long memberId;

    /**
     * 属性数据
     */
    private String attrMap;

}

package com.ruoyi.web.app.cost.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.web.app.cost.mapper.PmpProjectCostMapper;
import com.ruoyi.web.app.cost.service.IPmpProjectCostService;
import com.ruoyi.web.app.project.domain.PmpProjectInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 项目管理Service业务层处理
 *
 * @author zjy
 * @date 2024-04-23
 */
@Service
public class PmpProjectCostServiceImpl implements IPmpProjectCostService
{
    @Resource
    private PmpProjectCostMapper pmpProjectMapper;

    /**
     * 查询项目管理
     *
     * @param id 项目管理ID
     * @return 项目管理
     */
    @Override
    public PmpProjectInfo selectPmpProjectById(Long id)
    {
        return pmpProjectMapper.selectPmpProjectInfoById(id);
    }

    /**
     * 查询项目管理列表
     *
     * @param pmpProject 项目管理
     * @return 项目管理
     */
    @Override
    public List<PmpProjectInfo> selectPmpProjectList(PmpProjectInfo pmpProject)
    {
        return pmpProjectMapper.selectPmpProjectInfoList(pmpProject);
    }


}

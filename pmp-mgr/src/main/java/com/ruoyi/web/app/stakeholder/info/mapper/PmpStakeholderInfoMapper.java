package com.ruoyi.web.app.stakeholder.info.mapper;


import com.ruoyi.web.app.stakeholder.info.domain.PmpStakeholderInfo;

import java.util.List;

/**
 * 相关方信息Mapper接口
 *
 * @author ruoyi
 * @date 2024-07-21
 */
public interface PmpStakeholderInfoMapper
{
    /**
     * 查询相关方信息
     *
     * @param id 相关方信息ID
     * @return 相关方信息
     */
    public PmpStakeholderInfo selectPmpStakeholderInfoById(Long id);

    /**
     * 查询相关方信息列表
     *
     * @param pmpStakeholderInfo 相关方信息
     * @return 相关方信息集合
     */
    List<PmpStakeholderInfo> selectPmpStakeholderInfoList(PmpStakeholderInfo pmpStakeholderInfo);

    /**
     * 新增相关方信息
     *
     * @param pmpStakeholderInfo 相关方信息
     * @return 结果
     */
    int insertPmpStakeholderInfo(PmpStakeholderInfo pmpStakeholderInfo);

    /**
     * 修改相关方信息
     *
     * @param pmpStakeholderInfo 相关方信息
     * @return 结果
     */
    int updatePmpStakeholderInfo(PmpStakeholderInfo pmpStakeholderInfo);

    /**
     * 删除相关方信息
     *
     * @param id 相关方信息ID
     * @return 结果
     */
    int deletePmpStakeholderInfoById(Long id);

    /**
     * 批量删除相关方信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deletePmpStakeholderInfoByIds(String[] ids);
}

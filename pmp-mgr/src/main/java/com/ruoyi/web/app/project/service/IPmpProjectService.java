package com.ruoyi.web.app.project.service;


import com.ruoyi.web.app.project.domain.PmpProjectInfo;

import java.util.List;

/**
 * 项目管理Service接口
 *
 * @author zjy
 * @date 2024-04-23
 */
public interface IPmpProjectService
{
    /**
     * 查询项目管理
     *
     * @param id 项目管理ID
     * @return 项目管理
     */
    public PmpProjectInfo selectPmpProjectById(Long id);

    /**
     * 查询项目管理列表
     *
     * @param pmpProject 项目管理
     * @return 项目管理集合
     */
    public List<PmpProjectInfo> selectPmpProjectList(PmpProjectInfo pmpProject);

    /**
     * 新增项目管理
     *
     * @param pmpProject 项目管理
     * @return 结果
     */
    public int insertPmpProject(PmpProjectInfo pmpProject);

    /**
     * 修改项目管理
     *
     * @param pmpProject 项目管理
     * @return 结果
     */
    public int updatePmpProject(PmpProjectInfo pmpProject);

    /**
     * 批量删除项目管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePmpProjectByIds(String ids);

    /**
     * 删除项目管理信息
     *
     * @param id 项目管理ID
     * @return 结果
     */
    public int deletePmpProjectById(Long id);
}

package com.ruoyi.web.app.cost.mapper;


import com.ruoyi.web.app.project.domain.PmpProjectInfo;

import java.util.List;

/**
 * 项目成本管理Mapper接口
 *
 * @author zjy
 * @date 2024-04-23
 */
public interface PmpProjectCostMapper
{
    /**
     * 查询项目管理
     *
     * @param id 项目管理ID
     * @return 项目管理
     */
    PmpProjectInfo selectPmpProjectInfoById(Long id);

    /**
     * 查询项目管理列表
     *
     * @param pmpProject 项目管理
     * @return 项目管理集合
     */
    List<PmpProjectInfo> selectPmpProjectInfoList(PmpProjectInfo pmpProject);

}

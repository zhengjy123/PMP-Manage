package com.ruoyi.web.app.stakeholder.info.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 相关方信息对象 pmp_stakeholder_info
 *
 * @author ruoyi
 * @date 2024-07-21
 */
@Data
public class PmpStakeholderInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 项目id */
    @Excel(name = "项目id")
    private Long projectId;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String projectName;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 角色 */
    @Excel(name = "角色")
    private String role;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobile;

    /** 主要需求 */
    @Excel(name = "主要需求")
    private String requirement;

    /** 主要期望 */
    @Excel(name = "主要期望")
    private String respect;

    /** 主要影响 */
    @Excel(name = "主要影响")
    private String influence;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 优先级 */
    @Excel(name = "优先级")
    private Long priority;

    /** 单位或组织 */
    @Excel(name = "单位或组织")
    private String company;

    /** 岗位 */
    @Excel(name = "岗位")
    private String post;

}

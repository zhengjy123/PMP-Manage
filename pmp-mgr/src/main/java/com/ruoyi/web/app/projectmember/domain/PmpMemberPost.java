package com.ruoyi.web.app.projectmember.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 成员和岗位关联 sys_user_post
 *
 * @author ruoyi
 */
@Data
public class PmpMemberPost
{
    /** 成员ID */
    private Long memberId;

    /** 岗位ID */
    private Long postId;

}

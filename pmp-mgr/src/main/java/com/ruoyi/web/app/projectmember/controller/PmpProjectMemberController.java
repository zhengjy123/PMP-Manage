package com.ruoyi.web.app.projectmember.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.app.projectmember.domain.PmpMember;
import com.ruoyi.web.app.projectmember.service.IPmpMemberService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 项目成员管理控制器
 *
 * <p>描述信息</p>
 *
 * @version 1.0
 * @see PmpMember
 * @author zjy
 * 2024-04-23
 */
@Controller
@RequestMapping("/system/project/member")
public class PmpProjectMemberController extends BaseController
{
    private String prefix = "app/project_member";

    @Resource
    private IPmpMemberService iPmpMemberService;

    @RequiresPermissions("system:project:view")
    @GetMapping()
    public String project()
    {
        return prefix + "/list";
    }


    /**
     * 查询项目成员列表
     * @param pmpMember 查询条件
     * @return 分页信息
     */
    @RequiresPermissions("system:project:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmpMember pmpMember)
    {
        startPage();
        List<PmpMember> list = iPmpMemberService.selectPmpMemberList(pmpMember);
        return getDataTable(list);
    }

    /**
     * 导出项目成员列表
     * @param pmpMember 查询条件
     * @return 前端页面路径
     */
    @RequiresPermissions("system:project:export")
    @Log(title = "项目管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmpMember pmpMember)
    {
        List<PmpMember> list = iPmpMemberService.selectPmpMemberList(pmpMember);
        ExcelUtil<PmpMember> util = new ExcelUtil<>(PmpMember.class);
        return util.exportExcel(list, "project");
    }

    /**
     *  新增项目成员路由
     *  @return 前端页面路径
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存项目成员
     * @param pmpMember 提交对象
     * @return 封装的返回对象
     */
    @RequiresPermissions("system:project:add")
    @Log(title = "项目管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmpMember pmpMember)
    {
        return toAjax(iPmpMemberService.insertPmpMember(pmpMember));
    }

    /**
     * 修改项目成员
     * @param id 主键id
     * @param modelMap
     * @return 封装的返回对象
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap modelMap)
    {
        PmpMember pmpMember = iPmpMemberService.selectPmpMemberById(id);
        modelMap.put("pmpMember", pmpMember);
        return prefix + "/edit";
    }

    /**
     * 修改保存项目成员
     * @param pmpMember 提交对象
     * @return 封装的返回对象
     */
    @RequiresPermissions("system:project:edit")
    @Log(title = "项目成员管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmpMember pmpMember)
    {
        return toAjax(iPmpMemberService.updatePmpMember(pmpMember));
    }

    /**
     * 删除项目成员
     * @param ids 主键集合
     * @return 封装的返回对象
     */
    @RequiresPermissions("system:project:remove")
    @Log(title = "项目管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(iPmpMemberService.deletePmpMemberByIds(ids));
    }

    /**
     * 项目成员明细
     * @param memberId
     * @param mmap
     * @return
     */
    @RequiresPermissions("system:project:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long memberId, ModelMap mmap)
    {
        mmap.put("member", iPmpMemberService.selectPmpMemberDetail(memberId));
        return prefix + "/detail";
    }
}

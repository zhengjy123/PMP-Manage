package com.ruoyi.web.app.scope.requirement.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.web.app.scope.requirement.domain.PmpScopeRequirement;
import com.ruoyi.web.app.scope.requirement.mapper.PmpScopeRequirementMapper;
import com.ruoyi.web.app.scope.requirement.service.IPmpScopeRequirementService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 范围管理-需求管理Service业务层处理
 *
 * @author zjy
 * @date 2024-07-30
 */
@Service
public class PmpScopeRequirementServiceImpl implements IPmpScopeRequirementService
{
    @Resource
    private PmpScopeRequirementMapper pmpScopeRequirementMapper;

    /**
     * 查询范围管理-需求管理
     *
     * @param id 范围管理-需求管理ID
     * @return 范围管理-需求管理
     */
    @Override
    public PmpScopeRequirement selectPmpScopeRequirementById(Long id)
    {
        return pmpScopeRequirementMapper.selectPmpScopeRequirementById(id);
    }

    /**
     * 查询范围管理-需求管理列表
     *
     * @param pmpScopeRequirement 范围管理-需求管理
     * @return 范围管理-需求管理
     */
    @Override
    public List<PmpScopeRequirement> selectPmpScopeRequirementList(PmpScopeRequirement pmpScopeRequirement)
    {
        return pmpScopeRequirementMapper.selectPmpScopeRequirementList(pmpScopeRequirement);
    }

    /**
     * 新增范围管理-需求管理
     *
     * @param pmpScopeRequirement 范围管理-需求管理
     * @return 结果
     */
    @Override
    public int insertPmpScopeRequirement(PmpScopeRequirement pmpScopeRequirement)
    {
        return pmpScopeRequirementMapper.insertPmpScopeRequirement(pmpScopeRequirement);
    }

    /**
     * 修改范围管理-需求管理
     *
     * @param pmpScopeRequirement 范围管理-需求管理
     * @return 结果
     */
    @Override
    public int updatePmpScopeRequirement(PmpScopeRequirement pmpScopeRequirement)
    {
        return pmpScopeRequirementMapper.updatePmpScopeRequirement(pmpScopeRequirement);
    }

    /**
     * 删除范围管理-需求管理对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmpScopeRequirementByIds(String ids)
    {
        return pmpScopeRequirementMapper.deletePmpScopeRequirementByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除范围管理-需求管理信息
     *
     * @param id 范围管理-需求管理ID
     * @return 结果
     */
    @Override
    public int deletePmpScopeRequirementById(Long id)
    {
        return pmpScopeRequirementMapper.deletePmpScopeRequirementById(id);
    }
}

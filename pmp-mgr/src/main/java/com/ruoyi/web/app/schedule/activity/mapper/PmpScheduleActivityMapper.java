package com.ruoyi.web.app.schedule.activity.mapper;


import com.ruoyi.web.app.schedule.activity.domain.PmpScheduleActivity;

import java.util.List;

/**
 * 项目进度管理-活动清单Mapper接口
 *
 * @author zjy
 * @date 2024-07-18
 */
public interface PmpScheduleActivityMapper
{
    /**
     * 查询项目进度管理-活动清单
     *
     * @param id 项目进度管理-活动清单ID
     * @return 项目进度管理-活动清单
     */
    public PmpScheduleActivity selectPmpScheduleActivityById(Long id);

    /**
     * 查询项目进度管理-活动清单列表
     *
     * @param pmpScheduleActivity 项目进度管理-活动清单
     * @return 项目进度管理-活动清单集合
     */
    List<PmpScheduleActivity> selectPmpScheduleActivityList(PmpScheduleActivity pmpScheduleActivity);

    /**
     * 新增项目进度管理-活动清单
     *
     * @param pmpScheduleActivity 项目进度管理-活动清单
     * @return 结果
     */
    int insertPmpScheduleActivity(PmpScheduleActivity pmpScheduleActivity);

    /**
     * 修改项目进度管理-活动清单
     *
     * @param pmpScheduleActivity 项目进度管理-活动清单
     * @return 结果
     */
    int updatePmpScheduleActivity(PmpScheduleActivity pmpScheduleActivity);

    /**
     * 删除项目进度管理-活动清单
     *
     * @param id 项目进度管理-活动清单ID
     * @return 结果
     */
    int deletePmpScheduleActivityById(Long id);

    /**
     * 批量删除项目进度管理-活动清单
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deletePmpScheduleActivityByIds(String[] ids);
}

package com.ruoyi.web.app.quality.bug.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 缺陷管理对象 pmp_quality_bug
 *
 * @author zjy
 * @date 2024-07-31
 */
@Data
public class PmpQualityBug extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识符 */
    private Long id;

    /** 缺陷标题 */
    @Excel(name = "缺陷标题")
    private String title;

    /** 缺陷描述 */
    @Excel(name = "缺陷描述")
    private String description;

    /** 缺陷描述图片 */
    @Excel(name = "缺陷描述图片")
    private String image;

    /** 项目ID */
    @Excel(name = "项目ID")
    private Long projectId;

    /** 处理人 */
    @Excel(name = "处理人")
    private String assignee;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 严重程度 */
    @Excel(name = "严重程度")
    private Long severity;

    /** 解决方案 */
    @Excel(name = "解决方案")
    private String solution;

    /** 缺陷产生原因 */
    @Excel(name = "缺陷产生原因")
    private String causedBy;

}

package com.ruoyi.web.app.risk.register.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotBlank;

/**
 * 风险登记册对象 pmp_risk_register
 *
 * @author zjy
 * @date 2024-07-18
 */
@Data
public class PmpRiskRegister extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 项目id */
    @Excel(name = "项目id")
    private Long projectId;

    /** 风险名称 */
    @Excel(name = "风险名称")
    private String name;

    /** 风险描述 */
    @Excel(name = "风险描述")
    private String description;

    /** 风险类型 */
    @Excel(name = "风险类型")
    @NotBlank(message = "风险类型不能为空")
    private Integer type;

    /** 风险来源 */
    @Excel(name = "风险来源")
    private Long source;

    /** 事件 */
    @Excel(name = "事件")
    private String event;

    /** 后果 */
    @Excel(name = "后果")
    private String consequence;

    /** 发生概率 */
    @Excel(name = "发生概率")
    private Double probability;

    /** 责任人id */
    @Excel(name = "责任人id")
    private Long directorId;

    /** 责任人名称 */
    @Excel(name = "责任人名称")
    private String directorName;

    /** 应对策略 */
    private String responseStrategy;

}

package com.ruoyi.web.app.quality.bug.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.app.quality.bug.domain.PmpQualityBug;
import com.ruoyi.web.app.quality.bug.service.IPmpQualityBugService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 缺陷管理Controller
 *
 * @author zjy
 * 2024-07-31
 */
@Controller
@RequestMapping("/system/bug")
public class PmpQualityBugController extends BaseController
{
    private String prefix = "app/quality_bug";

    @Resource
    private IPmpQualityBugService pmpQualityBugService;

    @RequiresPermissions("system:bug:view")
    @GetMapping()
    public String bug()
    {
        return prefix + "/list";
    }

    /**
     * 查询缺陷管理列表
     */
    @RequiresPermissions("system:bug:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmpQualityBug pmpQualityBug)
    {
        startPage();
        List<PmpQualityBug> list = pmpQualityBugService.selectPmpQualityBugList(pmpQualityBug);
        return getDataTable(list);
    }

    /**
     * 导出缺陷管理列表
     */
    @RequiresPermissions("system:bug:export")
    @Log(title = "缺陷管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmpQualityBug pmpQualityBug)
    {
        List<PmpQualityBug> list = pmpQualityBugService.selectPmpQualityBugList(pmpQualityBug);
        ExcelUtil<PmpQualityBug> util = new ExcelUtil<PmpQualityBug>(PmpQualityBug.class);
        return util.exportExcel(list, "bug");
    }

    /**
     * 新增缺陷管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存缺陷管理
     */
    @RequiresPermissions("system:bug:add")
    @Log(title = "缺陷管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmpQualityBug pmpQualityBug)
    {
        return toAjax(pmpQualityBugService.insertPmpQualityBug(pmpQualityBug));
    }

    /**
     * 修改缺陷管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PmpQualityBug pmpQualityBug = pmpQualityBugService.selectPmpQualityBugById(id);
        mmap.put("pmpQualityBug", pmpQualityBug);
        return prefix + "/edit";
    }

    /**
     * 修改保存缺陷管理
     */
    @RequiresPermissions("system:bug:edit")
    @Log(title = "缺陷管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmpQualityBug pmpQualityBug)
    {
        return toAjax(pmpQualityBugService.updatePmpQualityBug(pmpQualityBug));
    }

    /**
     * 删除缺陷管理
     */
    @RequiresPermissions("system:bug:remove")
    @Log(title = "缺陷管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pmpQualityBugService.deletePmpQualityBugByIds(ids));
    }
}

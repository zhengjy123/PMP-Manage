package com.ruoyi.web.app.stakeholder.info.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.web.app.stakeholder.info.domain.PmpStakeholderInfo;
import com.ruoyi.web.app.stakeholder.info.mapper.PmpStakeholderInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.web.app.stakeholder.info.service.IPmpStakeholderInfoService;
import javax.annotation.Resource;

/**
 * 相关方信息Service业务层处理
 *
 * @author ruoyi
 * @date 2024-07-21
 */
@Service
public class PmpStakeholderInfoServiceImpl implements IPmpStakeholderInfoService
{
    @Resource
    private PmpStakeholderInfoMapper pmpStakeholderInfoMapper;

    /**
     * 查询相关方信息
     *
     * @param id 相关方信息ID
     * @return 相关方信息
     */
    @Override
    public PmpStakeholderInfo selectPmpStakeholderInfoById(Long id)
    {
        return pmpStakeholderInfoMapper.selectPmpStakeholderInfoById(id);
    }

    /**
     * 查询相关方信息列表
     *
     * @param pmpStakeholderInfo 相关方信息
     * @return 相关方信息
     */
    @Override
    public List<PmpStakeholderInfo> selectPmpStakeholderInfoList(PmpStakeholderInfo pmpStakeholderInfo)
    {
        return pmpStakeholderInfoMapper.selectPmpStakeholderInfoList(pmpStakeholderInfo);
    }

    /**
     * 新增相关方信息
     *
     * @param pmpStakeholderInfo 相关方信息
     * @return 结果
     */
    @Override
    public int insertPmpStakeholderInfo(PmpStakeholderInfo pmpStakeholderInfo)
    {
        pmpStakeholderInfo.setCreateTime(DateUtils.getNowDate());
        return pmpStakeholderInfoMapper.insertPmpStakeholderInfo(pmpStakeholderInfo);
    }

    /**
     * 修改相关方信息
     *
     * @param pmpStakeholderInfo 相关方信息
     * @return 结果
     */
    @Override
    public int updatePmpStakeholderInfo(PmpStakeholderInfo pmpStakeholderInfo)
    {
        return pmpStakeholderInfoMapper.updatePmpStakeholderInfo(pmpStakeholderInfo);
    }

    /**
     * 删除相关方信息对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmpStakeholderInfoByIds(String ids)
    {
        return pmpStakeholderInfoMapper.deletePmpStakeholderInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除相关方信息信息
     *
     * @param id 相关方信息ID
     * @return 结果
     */
    @Override
    public int deletePmpStakeholderInfoById(Long id)
    {
        return pmpStakeholderInfoMapper.deletePmpStakeholderInfoById(id);
    }
}

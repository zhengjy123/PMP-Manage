package com.ruoyi.web.app.projectmember.service;


import com.ruoyi.system.domain.SysPost;
import com.ruoyi.web.app.projectmember.domain.PmpMember;
import com.ruoyi.web.app.projectmember.vo.PmpMemberVO;

import java.util.List;

/**
 * 项目人员信息Service接口
 *
 * @author zjy
 * @date 2024-04-19
 */
public interface IPmpMemberService
{
    /**
     * 查询项目人员信息
     *
     * @param id 项目人员信息ID
     * @return 项目人员信息
     */
    PmpMember selectPmpMemberById(Long id);

    /**
     * 查询项目人员信息列表
     *
     * @param pmpMember 项目人员信息
     * @return 项目人员信息集合
     */
    List<PmpMember> selectPmpMemberList(PmpMember pmpMember);

    /**
     * 新增项目人员信息
     *
     * @param pmpMember 项目人员信息
     * @return 结果
     */
    int insertPmpMember(PmpMember pmpMember);

    /**
     * 修改项目人员信息
     *
     * @param pmpMember 项目人员信息
     * @return 结果
     */
    int updatePmpMember(PmpMember pmpMember);

    /**
     * 批量删除项目人员信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deletePmpMemberByIds(String ids);

    /**
     * 删除项目人员信息信息
     *
     * @param id 项目人员信息ID
     * @return 结果
     */
    int deletePmpMemberById(Long id);


    List<SysPost> selectPostsByMemberId(Long memberId);

    /**
     * 查询人员详细信息
     * @return
     */
    PmpMemberVO selectPmpMemberDetail(Long memberId);
}

package com.ruoyi.web.app.risk.register.controller;

import java.util.List;

import com.ruoyi.web.app.risk.register.domain.PmpRiskRegister;
import com.ruoyi.web.app.risk.register.service.IPmpRiskRegisterService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.annotation.Resource;

/**
 * 风险登记册Controller
 *
 * @author zjy
 * @date 2024-07-18
 */
@Controller
@RequestMapping("/system/register")
public class PmpRiskRegisterController extends BaseController
{
    private String prefix = "app/risk_register";

    @Resource
    private IPmpRiskRegisterService pmpRiskRegisterService;

    @RequiresPermissions("system:register:view")
    @GetMapping()
    public String register()
    {
        return prefix + "/list";
    }

    /**
     * 查询风险登记册列表
     */
    @RequiresPermissions("system:register:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmpRiskRegister pmpRiskRegister)
    {
        startPage();
        List<PmpRiskRegister> list = pmpRiskRegisterService.selectPmpRiskRegisterList(pmpRiskRegister);
        return getDataTable(list);
    }

    /**
     * 导出风险登记册列表
     */
    @RequiresPermissions("system:register:export")
    @Log(title = "风险登记册", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmpRiskRegister pmpRiskRegister)
    {
        List<PmpRiskRegister> list = pmpRiskRegisterService.selectPmpRiskRegisterList(pmpRiskRegister);
        ExcelUtil<PmpRiskRegister> util = new ExcelUtil<PmpRiskRegister>(PmpRiskRegister.class);
        return util.exportExcel(list, "register");
    }

    /**
     * 新增风险登记册
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存风险登记册
     */
    @RequiresPermissions("system:register:add")
    @Log(title = "风险登记册", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated PmpRiskRegister pmpRiskRegister)
    {
        return toAjax(pmpRiskRegisterService.insertPmpRiskRegister(pmpRiskRegister));
    }

    /**
     * 修改风险登记册
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PmpRiskRegister pmpRiskRegister = pmpRiskRegisterService.selectPmpRiskRegisterById(id);
        mmap.put("pmpRiskRegister", pmpRiskRegister);
        return prefix + "/edit";
    }

    /**
     * 修改保存风险登记册
     */
    @RequiresPermissions("system:register:edit")
    @Log(title = "风险登记册", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmpRiskRegister pmpRiskRegister)
    {
        return toAjax(pmpRiskRegisterService.updatePmpRiskRegister(pmpRiskRegister));
    }

    /**
     * 删除风险登记册
     */
    @RequiresPermissions("system:register:remove")
    @Log(title = "风险登记册", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pmpRiskRegisterService.deletePmpRiskRegisterByIds(ids));
    }
}

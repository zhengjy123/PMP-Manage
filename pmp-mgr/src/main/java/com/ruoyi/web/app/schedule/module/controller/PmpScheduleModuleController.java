package com.ruoyi.web.app.schedule.module.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.app.schedule.module.domain.PmpScheduleModule;
import com.ruoyi.web.app.schedule.module.service.IPmpScheduleModuleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 进度-模块管理Controller
 *
 * @author zjy
 * @date 2024-08-25
 */
@Controller
@RequestMapping("/system/module")
public class PmpScheduleModuleController extends BaseController
{
    private String prefix = "app/module";

    @Resource
    private IPmpScheduleModuleService pmpScheduleModuleService;

    @RequiresPermissions("system:module:view")
    @GetMapping()
    public String module()
    {
        return prefix + "/list";
    }

    /**
     * 查询进度-模块管理列表
     */
    @RequiresPermissions("system:module:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmpScheduleModule pmpScheduleModule)
    {
        startPage();
        List<PmpScheduleModule> list = pmpScheduleModuleService.selectPmpScheduleModuleList(pmpScheduleModule);
        return getDataTable(list);
    }

    /**
     * 导出进度-模块管理列表
     */
    @RequiresPermissions("system:module:export")
    @Log(title = "进度-模块管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmpScheduleModule pmpScheduleModule)
    {
        List<PmpScheduleModule> list = pmpScheduleModuleService.selectPmpScheduleModuleList(pmpScheduleModule);
        ExcelUtil<PmpScheduleModule> util = new ExcelUtil<PmpScheduleModule>(PmpScheduleModule.class);
        return util.exportExcel(list, "module");
    }

    /**
     * 新增进度-模块管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存进度-模块管理
     */
    @RequiresPermissions("system:module:add")
    @Log(title = "进度-模块管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmpScheduleModule pmpScheduleModule)
    {
        return toAjax(pmpScheduleModuleService.insertPmpScheduleModule(pmpScheduleModule));
    }

    /**
     * 修改进度-模块管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PmpScheduleModule pmpScheduleModule = pmpScheduleModuleService.selectPmpScheduleModuleById(id);
        mmap.put("pmpScheduleModule", pmpScheduleModule);
        return prefix + "/edit";
    }

    /**
     * 修改保存进度-模块管理
     */
    @RequiresPermissions("system:module:edit")
    @Log(title = "进度-模块管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmpScheduleModule pmpScheduleModule)
    {
        return toAjax(pmpScheduleModuleService.updatePmpScheduleModule(pmpScheduleModule));
    }

    /**
     * 删除进度-模块管理
     */
    @RequiresPermissions("system:module:remove")
    @Log(title = "进度-模块管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pmpScheduleModuleService.deletePmpScheduleModuleByIds(ids));
    }
}

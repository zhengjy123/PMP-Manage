package com.ruoyi.web.app.risk.register.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.web.app.risk.register.domain.PmpRiskRegister;
import com.ruoyi.web.app.risk.register.mapper.PmpRiskRegisterMapper;
import com.ruoyi.web.app.risk.register.service.IPmpRiskRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.core.text.Convert;

import javax.annotation.Resource;
import java.util.List;

/**
 * 风险登记册Service业务层处理
 *
 * @author zjy
 * @date 2024-07-18
 */
@Service
public class PmpRiskRegisterServiceImpl implements IPmpRiskRegisterService
{
    @Resource
    private PmpRiskRegisterMapper pmpRiskRegisterMapper;

    /**
     * 查询风险登记册
     *
     * @param id 风险登记册ID
     * @return 风险登记册
     */
    @Override
    public PmpRiskRegister selectPmpRiskRegisterById(Long id)
    {
        return pmpRiskRegisterMapper.selectPmpRiskRegisterById(id);
    }

    /**
     * 查询风险登记册列表
     *
     * @param pmpRiskRegister 风险登记册
     * @return 风险登记册
     */
    @Override
    public List<PmpRiskRegister> selectPmpRiskRegisterList(PmpRiskRegister pmpRiskRegister)
    {
        return pmpRiskRegisterMapper.selectPmpRiskRegisterList(pmpRiskRegister);
    }

    /**
     * 新增风险登记册
     *
     * @param pmpRiskRegister 风险登记册
     * @return 结果
     */
    @Override
    public int insertPmpRiskRegister(PmpRiskRegister pmpRiskRegister)
    {
        pmpRiskRegister.setCreateTime(DateUtils.getNowDate());
        return pmpRiskRegisterMapper.insertPmpRiskRegister(pmpRiskRegister);
    }

    /**
     * 修改风险登记册
     *
     * @param pmpRiskRegister 风险登记册
     * @return 结果
     */
    @Override
    public int updatePmpRiskRegister(PmpRiskRegister pmpRiskRegister)
    {
        return pmpRiskRegisterMapper.updatePmpRiskRegister(pmpRiskRegister);
    }

    /**
     * 删除风险登记册对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmpRiskRegisterByIds(String ids)
    {
        return pmpRiskRegisterMapper.deletePmpRiskRegisterByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除风险登记册信息
     *
     * @param id 风险登记册ID
     * @return 结果
     */
    @Override
    public int deletePmpRiskRegisterById(Long id)
    {
        return pmpRiskRegisterMapper.deletePmpRiskRegisterById(id);
    }
}

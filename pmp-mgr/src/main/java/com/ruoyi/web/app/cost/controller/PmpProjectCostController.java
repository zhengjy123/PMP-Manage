package com.ruoyi.web.app.cost.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.app.cost.service.IPmpProjectCostService;
import com.ruoyi.web.app.project.domain.PmpProjectInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * 项目成本管理Controller
 *
 * @author zjy
 * @date 2024-04-23
 */
@Controller
@RequestMapping("/system/project/cost")
public class PmpProjectCostController extends BaseController
{
    private String prefix = "app/cost";

    @Resource
    private IPmpProjectCostService pmpProjectService;

    @RequiresPermissions("system:project:view")
    @GetMapping()
    public String project()
    {
        return prefix + "/list";
    }

    /**
     * 查询项目管理列表
     */
    @RequiresPermissions("system:project:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmpProjectInfo pmpProject)
    {
        startPage();
        List<PmpProjectInfo> list = pmpProjectService.selectPmpProjectList(pmpProject);
        return getDataTable(list);
    }

    /**
     * 导出项目管理列表
     */
    @RequiresPermissions("system:project:export")
    @Log(title = "项目管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmpProjectInfo pmpProject)
    {
        List<PmpProjectInfo> list = pmpProjectService.selectPmpProjectList(pmpProject);
        ExcelUtil<PmpProjectInfo> util = new ExcelUtil<>(PmpProjectInfo.class);
        return util.exportExcel(list, "project");
    }



}

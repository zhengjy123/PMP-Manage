package com.ruoyi.web.app.quality.bug.mapper;


import com.ruoyi.web.app.quality.bug.domain.PmpQualityBug;

import java.util.List;

/**
 * 缺陷管理Mapper接口
 *
 * @author zjy
 * @date 2024-07-31
 */
public interface PmpQualityBugMapper
{
    /**
     * 查询缺陷管理
     *
     * @param id 缺陷管理ID
     * @return 缺陷管理
     */
    public PmpQualityBug selectPmpQualityBugById(Long id);

    /**
     * 查询缺陷管理列表
     *
     * @param pmpQualityBug 缺陷管理
     * @return 缺陷管理集合
     */
    List<PmpQualityBug> selectPmpQualityBugList(PmpQualityBug pmpQualityBug);

    /**
     * 新增缺陷管理
     *
     * @param pmpQualityBug 缺陷管理
     * @return 结果
     */
    int insertPmpQualityBug(PmpQualityBug pmpQualityBug);

    /**
     * 修改缺陷管理
     *
     * @param pmpQualityBug 缺陷管理
     * @return 结果
     */
    int updatePmpQualityBug(PmpQualityBug pmpQualityBug);

    /**
     * 删除缺陷管理
     *
     * @param id 缺陷管理ID
     * @return 结果
     */
    int deletePmpQualityBugById(Long id);

    /**
     * 批量删除缺陷管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deletePmpQualityBugByIds(String[] ids);
}

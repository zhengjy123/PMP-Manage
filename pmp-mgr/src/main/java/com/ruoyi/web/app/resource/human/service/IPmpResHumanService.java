package com.ruoyi.web.app.resource.human.service;

import com.ruoyi.web.app.resource.human.domain.PmpResHuman;
import com.ruoyi.web.app.resource.human.pojo.PmpResHumanVO;

import java.util.List;

/**
 * 资源管理 - 人力资源管理Service接口
 *
 * @author zjy
 * @date 2024-08-26
 */
public interface IPmpResHumanService
{
    /**
     * 查询资源管理 - 人力资源管理
     *
     * @param id 资源管理 - 人力资源管理ID
     * @return 资源管理 - 人力资源管理
     */
    PmpResHuman selectPmpResHumanById(Long id);

    /**
     * 查询资源管理 - 人力资源管理列表
     *
     * @param pmpResHuman 资源管理 - 人力资源管理
     * @return 资源管理 - 人力资源管理集合
     */
    List<PmpResHuman> selectPmpResHumanList(PmpResHuman pmpResHuman);

    /**
     * 新增资源管理 - 人力资源管理
     *
     * @param pmpResHuman 资源管理 - 人力资源管理
     * @return 结果
     */
    int insertPmpResHuman(PmpResHuman pmpResHuman);

    /**
     * 修改资源管理 - 人力资源管理
     *
     * @param pmpResHuman 资源管理 - 人力资源管理
     * @return 结果
     */
    int updatePmpResHuman(PmpResHuman pmpResHuman);

    /**
     * 批量删除资源管理 - 人力资源管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deletePmpResHumanByIds(String ids);

    /**
     * 删除资源管理 - 人力资源管理信息
     *
     * @param id 资源管理 - 人力资源管理ID
     * @return 结果
     */
    int deletePmpResHumanById(Long id);

    /**
     * 查询人力资源信息以及附带项目基础信息
     * @param
     * @return
     */
    List<PmpResHumanVO> selectPmpResHumanProject(PmpResHuman pmpResHuman);
}

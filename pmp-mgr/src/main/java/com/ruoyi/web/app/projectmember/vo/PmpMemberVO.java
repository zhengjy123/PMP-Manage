package com.ruoyi.web.app.projectmember.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.SysPost;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 项目人员信息对象 pmp_member
 *
 * @author zjy
 * @date 2024-04-19
 */
@Data
public class PmpMemberVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 性别 字典:sys_user_sex */
    @Excel(name = "性别")
    private String sex;

    /** 出生日期 */
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;

    /** 邮箱地址 */
    @Excel(name = "邮箱地址")
    private String email;

    /** 手机号码 */
    @Excel(name = "手机号码")
    @NotBlank(message = "手机号码不能为空")
    private String phoneNumber;

    /** 创建人ID */
    @Excel(name = "创建人ID")
    private Long creatorId;

    /** 个人履历 */
    @Excel(name = "个人履历")
    private String personalHistory;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 岗位组 */
    private Long[] postIds;

    /** 查询到的岗位信息 */
    private List<SysPost> posts;

    /**
     * 属性原始数据
     */
    private String attrOriginMap;

    /**
     * 属性Map数据(方便前端解析)
     */
    private Map attrMap;

}

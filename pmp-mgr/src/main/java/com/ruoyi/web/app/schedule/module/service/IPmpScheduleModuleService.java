package com.ruoyi.web.app.schedule.module.service;

import com.ruoyi.web.app.schedule.module.domain.PmpScheduleModule;

import java.util.List;

/**
 * 进度-模块管理Service接口
 *
 * @author zjy
 * @date 2024-08-25
 */
public interface IPmpScheduleModuleService
{
    /**
     * 查询进度-模块管理
     *
     * @param id 进度-模块管理ID
     * @return 进度-模块管理
     */
    PmpScheduleModule selectPmpScheduleModuleById(Long id);

    /**
     * 查询进度-模块管理列表
     *
     * @param pmpScheduleModule 进度-模块管理
     * @return 进度-模块管理集合
     */
    List<PmpScheduleModule> selectPmpScheduleModuleList(PmpScheduleModule pmpScheduleModule);

    /**
     * 新增进度-模块管理
     *
     * @param pmpScheduleModule 进度-模块管理
     * @return 结果
     */
    int insertPmpScheduleModule(PmpScheduleModule pmpScheduleModule);

    /**
     * 修改进度-模块管理
     *
     * @param pmpScheduleModule 进度-模块管理
     * @return 结果
     */
    int updatePmpScheduleModule(PmpScheduleModule pmpScheduleModule);

    /**
     * 批量删除进度-模块管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deletePmpScheduleModuleByIds(String ids);

    /**
     * 删除进度-模块管理信息
     *
     * @param id 进度-模块管理ID
     * @return 结果
     */
    int deletePmpScheduleModuleById(Long id);
}

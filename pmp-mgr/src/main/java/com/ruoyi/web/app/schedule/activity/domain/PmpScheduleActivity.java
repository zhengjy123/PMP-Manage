package com.ruoyi.web.app.schedule.activity.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 项目进度管理-活动清单对象 pmp_schedule_activity
 *
 * @author zjy
 * @date 2024-07-18
 */
@Data
public class PmpScheduleActivity extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 活动编号 */
    @Excel(name = "活动编号")
    private String number;

    /** 活动名称 */
    @Excel(name = "活动名称")
    private String name;

    /** 活动描述 */
    @Excel(name = "活动描述")
    private String description;

    /** 父活动id */
    @Excel(name = "父活动id")
    private Long parentId;

    /** 负责人id */
    @Excel(name = "负责人id")
    private Long director;

    /** 负责人名称 */
    @Excel(name = "负责人名称")
    private String directorName;

    /** 交付成果id */
    @Excel(name = "交付成果id")
    private String deliveryResultId;

    /** 关注人列表 */
    @Excel(name = "关注人列表")
    private String focusIds;

    /** 发起人id */
    @Excel(name = "发起人id")
    private Long sponsorId;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 预估工时(人/天) */
    @Excel(name = "预估工时(人/天)")
    private Long workTime;

    /** 活动状态 */
    @Excel(name = "活动状态")
    private Integer status;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date beginTime;

    /** 结束时间 */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

}

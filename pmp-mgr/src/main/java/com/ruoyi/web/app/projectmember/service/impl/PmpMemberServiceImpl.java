package com.ruoyi.web.app.projectmember.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysPost;
import com.ruoyi.system.mapper.SysPostMapper;
import com.ruoyi.web.app.projectmember.domain.PmpMember;
import com.ruoyi.web.app.projectmember.domain.PmpMemberDetail;
import com.ruoyi.web.app.projectmember.domain.PmpMemberPost;
import com.ruoyi.web.app.projectmember.mapper.PmpMemberMapper;
import com.ruoyi.web.app.projectmember.mapper.PmpMemberPostMapper;
import com.ruoyi.web.app.projectmember.service.IPmpMemberService;
import com.ruoyi.web.app.projectmember.vo.PmpMemberVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 项目人员信息Service业务层处理
 *
 * @author zjy
 * @date 2024-04-19
 */
@Service
@Slf4j
public class PmpMemberServiceImpl implements IPmpMemberService
{
    @Resource
    private PmpMemberMapper pmpMemberMapper;

    @Resource
    private PmpMemberPostMapper pmpMemberPostMapper;

    @Resource
    private SysPostMapper sysPostMapper;

    /**
     * 查询项目人员信息
     *
     * @param id 项目人员信息ID
     * @return 项目人员信息
     */
    @Override
    public PmpMember selectPmpMemberById(Long id)
    {
        return pmpMemberMapper.selectPmpMemberById(id);
    }

    /**
     * 查询项目人员信息列表
     *
     * @param pmpMember 项目人员信息
     * @return 项目人员信息
     */
    @Override
    public List<PmpMember> selectPmpMemberList(PmpMember pmpMember)
    {
        List<PmpMember> result = pmpMemberMapper.selectPmpMemberList(pmpMember);
        return result;
    }

    /**
     * 新增项目人员信息
     *
     * @param pmpMember 项目人员信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertPmpMember(PmpMember pmpMember)
    {
        // 如果出生日期为空 则自动填写为 2000-01-01
        if (pmpMember.getBirthDate() == null) {
            pmpMember.setBirthDate(DateUtils.parseDate("2000-01-01"));
        }

        pmpMember.setCreatorId(ShiroUtils.getSysUser().getUserId());
        pmpMember.setCreateTime(DateUtils.getNowDate());
        int result = pmpMemberMapper.insertPmpMember(pmpMember);

        // 新增人员岗位关联
        insertMemberPost(pmpMember);

        return result;
    }

    /**
     * 新增人员岗位信息
     *
     * @param pmpMember 人员对象
     */
    public void insertMemberPost(PmpMember pmpMember)
    {
        Long[] posts = pmpMember.getPostIds();
        if (StringUtils.isNotNull(posts))
        {
            // 新增成员与岗位管理
            List<PmpMemberPost> list = new ArrayList<>();
            for (Long postId : posts)
            {
                log.debug("插入数据 {}", postId);
                PmpMemberPost memberPost = new PmpMemberPost();
                memberPost.setMemberId(pmpMember.getId());
                memberPost.setPostId(postId);
                list.add(memberPost);
            }
            if (list.size() > 0)
            {
                log.debug("批量插入表");
                pmpMemberPostMapper.batchAddMemberPost(list);
            }
        }
    }

    /**
     * 修改项目人员信息
     *
     * @param pmpMember 项目人员信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updatePmpMember(PmpMember pmpMember)
    {

        int result = pmpMemberMapper.updatePmpMember(pmpMember);

        // 删除用户与岗位关联
        pmpMemberPostMapper.deleteUserPostByUserId(pmpMember.getId());
        // 新增用户与岗位管理
        insertMemberPost(pmpMember);

        return result;
    }

    /**
     * 删除项目人员信息对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmpMemberByIds(String ids)
    {
        return pmpMemberMapper.deletePmpMemberByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除项目人员信息信息
     *
     * @param id 项目人员信息ID
     * @return 结果
     */
    @Override
    public int deletePmpMemberById(Long id)
    {
        return pmpMemberMapper.deletePmpMemberById(id);
    }

    /**
     * 根据用户ID查询岗位
     *
     * @param memberId 用户ID
     * @return 岗位列表
     */
    @Override
    public List<SysPost> selectPostsByMemberId(Long memberId)
    {
        List<SysPost> userPosts = sysPostMapper.selectPostsByMemberId(memberId);
        List<SysPost> posts = sysPostMapper.selectPostAll();

        for (SysPost post : posts)
        {
            for (SysPost userRole : userPosts)
            {
                if (post.getPostId().longValue() == userRole.getPostId().longValue())
                {
                    post.setFlag(true);
                    break;
                }
            }
        }
        return posts;
    }

    @Override
    public PmpMemberVO selectPmpMemberDetail(Long memberId) {

        PmpMemberVO vo = new PmpMemberVO();
        PmpMember pmpMember = pmpMemberMapper.selectPmpMemberById(memberId);
        BeanUtils.copyProperties(pmpMember,vo);
        if (pmpMember!=null){
            PmpMemberDetail detail = pmpMemberMapper.selectMemberDetail(pmpMember.getId());
            if (detail != null){
                vo.setAttrOriginMap(detail.getAttrMap());

                // 将字符串转map
                JSONObject paramsObj = JSONObject.parseObject(detail.getAttrMap());
                Map<String, Object> map = new HashMap<>();
                paramsObj.keySet().forEach(key -> map.put(key, paramsObj.get(key)));
                vo.setAttrMap(map);

            }

        }

        return vo;
    }

}

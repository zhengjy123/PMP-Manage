package com.ruoyi.web.app.project.mapper;


import com.ruoyi.web.app.project.domain.PmpProjectInfo;

import java.util.List;

/**
 * 项目管理Mapper接口
 *
 * @author zjy
 * @date 2024-04-23
 */
public interface PmpProjectInfoMapper
{
    /**
     * 查询项目管理
     *
     * @param id 项目管理ID
     * @return 项目管理
     */
    PmpProjectInfo selectPmpProjectInfoById(Long id);

    /**
     * 查询项目管理列表
     *
     * @param pmpProject 项目管理
     * @return 项目管理集合
     */
    List<PmpProjectInfo> selectPmpProjectInfoList(PmpProjectInfo pmpProject);

    /**
     * 新增项目管理
     *
     * @param pmpProject 项目管理
     * @return 结果
     */
    int insertPmpProjectInfo(PmpProjectInfo pmpProject);

    /**
     * 修改项目管理
     *
     * @param pmpProject 项目管理
     * @return 结果
     */
    int updatePmpProjectInfo(PmpProjectInfo pmpProject);

    /**
     * 删除项目管理
     *
     * @param id 项目管理ID
     * @return 结果
     */
    int deletePmpProjectInfoById(Long id);

    /**
     * 批量删除项目管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deletePmpProjectInfoByIds(String[] ids);

    /**
     * 判断名称是否重复
     * @param name
     * @return
     */
    boolean isProjectNameExists(String name);
}

package com.ruoyi.web.app.stakeholder.info.controller;

import java.util.List;

import com.ruoyi.web.app.stakeholder.info.domain.PmpStakeholderInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.web.app.stakeholder.info.service.IPmpStakeholderInfoService;
import javax.annotation.Resource;

/**
 * 相关方信息Controller
 *
 * @author ruoyi
 * @date 2024-07-21
 */
@Controller
@RequestMapping("/system/stakeholder/info")
public class PmpStakeholderInfoController extends BaseController
{
    private String prefix = "app/stakeholder_info";

    @Resource
    private IPmpStakeholderInfoService pmpStakeholderInfoService;

    @RequiresPermissions("system:info:view")
    @GetMapping()
    public String info()
    {
        return prefix + "/list";
    }

    /**
     * 查询相关方信息列表
     */
    @RequiresPermissions("system:info:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmpStakeholderInfo pmpStakeholderInfo)
    {
        startPage();
        List<PmpStakeholderInfo> list = pmpStakeholderInfoService.selectPmpStakeholderInfoList(pmpStakeholderInfo);
        return getDataTable(list);
    }

    /**
     * 导出相关方信息列表
     */
    @RequiresPermissions("system:info:export")
    @Log(title = "相关方信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmpStakeholderInfo pmpStakeholderInfo)
    {
        List<PmpStakeholderInfo> list = pmpStakeholderInfoService.selectPmpStakeholderInfoList(pmpStakeholderInfo);
        ExcelUtil<PmpStakeholderInfo> util = new ExcelUtil<PmpStakeholderInfo>(PmpStakeholderInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 新增相关方信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存相关方信息
     */
    @RequiresPermissions("system:info:add")
    @Log(title = "相关方信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmpStakeholderInfo pmpStakeholderInfo)
    {
        return toAjax(pmpStakeholderInfoService.insertPmpStakeholderInfo(pmpStakeholderInfo));
    }

    /**
     * 修改相关方信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PmpStakeholderInfo pmpStakeholderInfo = pmpStakeholderInfoService.selectPmpStakeholderInfoById(id);
        mmap.put("pmpStakeholderInfo", pmpStakeholderInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存相关方信息
     */
    @RequiresPermissions("system:info:edit")
    @Log(title = "相关方信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmpStakeholderInfo pmpStakeholderInfo)
    {
        return toAjax(pmpStakeholderInfoService.updatePmpStakeholderInfo(pmpStakeholderInfo));
    }

    /**
     * 删除相关方信息
     */
    @RequiresPermissions("system:info:remove")
    @Log(title = "相关方信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pmpStakeholderInfoService.deletePmpStakeholderInfoByIds(ids));
    }
}

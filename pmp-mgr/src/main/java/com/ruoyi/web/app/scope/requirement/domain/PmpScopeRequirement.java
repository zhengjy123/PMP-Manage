package com.ruoyi.web.app.scope.requirement.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 范围管理-需求管理对象 pmp_scope_requirement
 *
 * @author zjy
 * @date 2024-07-30
 */
@Data
public class PmpScopeRequirement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** null */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date beginTime;

    /** 结束时间 */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 需求负责人 */
    @Excel(name = "需求负责人")
    private String director;

    /** 优先级 */
    @Excel(name = "优先级")
    private Integer priority;

    /** 来源类型 */
    @Excel(name = "来源类型")
    private Integer sourceType;

    /** 关注人列表 */
    @Excel(name = "关注人列表")
    private String followerList;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

}

package com.ruoyi.web.app.risk.register.mapper;


import com.ruoyi.web.app.risk.register.domain.PmpRiskRegister;

import java.util.List;

/**
 * 风险登记册Mapper接口
 *
 * @author zjy
 * @date 2024-07-18
 */
public interface PmpRiskRegisterMapper
{
    /**
     * 查询风险登记册
     *
     * @param id 风险登记册ID
     * @return 风险登记册
     */
    public PmpRiskRegister selectPmpRiskRegisterById(Long id);

    /**
     * 查询风险登记册列表
     *
     * @param pmpRiskRegister 风险登记册
     * @return 风险登记册集合
     */
    List<PmpRiskRegister> selectPmpRiskRegisterList(PmpRiskRegister pmpRiskRegister);

    /**
     * 新增风险登记册
     *
     * @param pmpRiskRegister 风险登记册
     * @return 结果
     */
    int insertPmpRiskRegister(PmpRiskRegister pmpRiskRegister);

    /**
     * 修改风险登记册
     *
     * @param pmpRiskRegister 风险登记册
     * @return 结果
     */
    int updatePmpRiskRegister(PmpRiskRegister pmpRiskRegister);

    /**
     * 删除风险登记册
     *
     * @param id 风险登记册ID
     * @return 结果
     */
    int deletePmpRiskRegisterById(Long id);

    /**
     * 批量删除风险登记册
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deletePmpRiskRegisterByIds(String[] ids);
}

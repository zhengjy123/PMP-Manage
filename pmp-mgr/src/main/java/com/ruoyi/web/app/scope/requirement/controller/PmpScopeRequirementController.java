package com.ruoyi.web.app.scope.requirement.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.app.scope.requirement.domain.PmpScopeRequirement;
import com.ruoyi.web.app.scope.requirement.service.IPmpScopeRequirementService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 范围管理-需求管理Controller
 *
 * @author zjy
 * @date 2024-07-30
 */
@Controller
@RequestMapping("/system/requirement")
public class PmpScopeRequirementController extends BaseController
{
    // 前端文件目录路径
    private String prefix = "app/requirement";

    @Resource
    private IPmpScopeRequirementService pmpScopeRequirementService;

    @RequiresPermissions("system:requirement:view")
    @GetMapping()
    public String requirement()
    {
        return prefix + "/list";
    }

    /**
     * 查询范围管理-需求管理列表
     */
    @RequiresPermissions("system:requirement:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmpScopeRequirement pmpScopeRequirement)
    {
        startPage();
        List<PmpScopeRequirement> list = pmpScopeRequirementService.selectPmpScopeRequirementList(pmpScopeRequirement);
        return getDataTable(list);
    }

    /**
     * 导出范围管理-需求管理列表
     */
    @RequiresPermissions("system:requirement:export")
    @Log(title = "范围管理-需求管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmpScopeRequirement pmpScopeRequirement)
    {
        List<PmpScopeRequirement> list = pmpScopeRequirementService.selectPmpScopeRequirementList(pmpScopeRequirement);
        ExcelUtil<PmpScopeRequirement> util = new ExcelUtil<PmpScopeRequirement>(PmpScopeRequirement.class);
        return util.exportExcel(list, "requirement");
    }

    /**
     * 新增范围管理-需求管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存范围管理-需求管理
     */
    @RequiresPermissions("system:requirement:add")
    @Log(title = "范围管理-需求管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmpScopeRequirement pmpScopeRequirement)
    {
        return toAjax(pmpScopeRequirementService.insertPmpScopeRequirement(pmpScopeRequirement));
    }

    /**
     * 修改范围管理-需求管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PmpScopeRequirement pmpScopeRequirement = pmpScopeRequirementService.selectPmpScopeRequirementById(id);
        mmap.put("pmpScopeRequirement", pmpScopeRequirement);
        return prefix + "/edit";
    }

    /**
     * 修改保存范围管理-需求管理
     */
    @RequiresPermissions("system:requirement:edit")
    @Log(title = "范围管理-需求管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmpScopeRequirement pmpScopeRequirement)
    {
        return toAjax(pmpScopeRequirementService.updatePmpScopeRequirement(pmpScopeRequirement));
    }

    /**
     * 删除范围管理-需求管理
     */
    @RequiresPermissions("system:requirement:remove")
    @Log(title = "范围管理-需求管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pmpScopeRequirementService.deletePmpScopeRequirementByIds(ids));
    }
}

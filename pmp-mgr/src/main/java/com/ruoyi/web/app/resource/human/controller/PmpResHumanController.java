package com.ruoyi.web.app.resource.human.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.app.resource.human.domain.PmpResHuman;
import com.ruoyi.web.app.resource.human.pojo.PmpResHumanVO;
import com.ruoyi.web.app.resource.human.service.IPmpResHumanService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 资源管理 - 人力资源管理Controller
 *
 * @author zjy
 * @date 2024-08-26
 */
@Controller
@RequestMapping("/system/human")
public class PmpResHumanController extends BaseController
{
    private String prefix = "app/res_human";

    @Resource
    private IPmpResHumanService pmpResHumanService;

    @RequiresPermissions("system:human:view")
    @GetMapping()
    public String human()
    {
        return prefix + "/list";
    }

    /**
     * 查询资源管理 - 人力资源管理列表
     */
    @RequiresPermissions("system:human:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmpResHuman pmpResHuman)
    {
        startPage();
//        List<PmpResHuman> list = pmpResHumanService.selectPmpResHumanList(pmpResHuman);
//        return getDataTable(list);
        List<PmpResHumanVO> list = pmpResHumanService.selectPmpResHumanProject(pmpResHuman);
        return getDataTable(list);
    }



    /**
     * 导出资源管理 - 人力资源管理列表
     */
    @RequiresPermissions("system:human:export")
    @Log(title = "资源管理 - 人力资源管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmpResHuman pmpResHuman)
    {
        List<PmpResHuman> list = pmpResHumanService.selectPmpResHumanList(pmpResHuman);
        ExcelUtil<PmpResHuman> util = new ExcelUtil<PmpResHuman>(PmpResHuman.class);
        return util.exportExcel(list, "human");
    }

    /**
     * 新增资源管理 - 人力资源管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存资源管理 - 人力资源管理
     */
    @RequiresPermissions("system:human:add")
    @Log(title = "资源管理 - 人力资源管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmpResHuman pmpResHuman)
    {
        return toAjax(pmpResHumanService.insertPmpResHuman(pmpResHuman));
    }

    /**
     * 修改资源管理 - 人力资源管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PmpResHuman pmpResHuman = pmpResHumanService.selectPmpResHumanById(id);
        mmap.put("pmpResHuman", pmpResHuman);
        return prefix + "/edit";
    }

    /**
     * 修改保存资源管理 - 人力资源管理
     */
    @RequiresPermissions("system:human:edit")
    @Log(title = "资源管理 - 人力资源管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmpResHuman pmpResHuman)
    {
        return toAjax(pmpResHumanService.updatePmpResHuman(pmpResHuman));
    }

    /**
     * 删除资源管理 - 人力资源管理
     */
    @RequiresPermissions("system:human:remove")
    @Log(title = "资源管理 - 人力资源管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pmpResHumanService.deletePmpResHumanByIds(ids));
    }
}

package com.ruoyi.web.app.resource.officesupply.service;

import java.util.List;
import com.ruoyi.web.app.resource.officesupply.domain.PmpResOfficeSupply;

/**
 * 资源管理 - 办公用品管理Service接口
 *
 * @author ruoyi
 * @date 2024-07-20
 */
public interface IPmpResOfficeSupplyService
{
    /**
     * 查询资源管理 - 办公用品管理
     *
     * @param id 资源管理 - 办公用品管理ID
     * @return 资源管理 - 办公用品管理
     */
    PmpResOfficeSupply selectPmpResOfficeSupplyById(Long id);

    /**
     * 查询资源管理 - 办公用品管理列表
     *
     * @param pmpResOfficeSupply 资源管理 - 办公用品管理
     * @return 资源管理 - 办公用品管理集合
     */
    List<PmpResOfficeSupply> selectPmpResOfficeSupplyList(PmpResOfficeSupply pmpResOfficeSupply);

    /**
     * 新增资源管理 - 办公用品管理
     *
     * @param pmpResOfficeSupply 资源管理 - 办公用品管理
     * @return 结果
     */
    int insertPmpResOfficeSupply(PmpResOfficeSupply pmpResOfficeSupply);

    /**
     * 修改资源管理 - 办公用品管理
     *
     * @param pmpResOfficeSupply 资源管理 - 办公用品管理
     * @return 结果
     */
    int updatePmpResOfficeSupply(PmpResOfficeSupply pmpResOfficeSupply);

    /**
     * 批量删除资源管理 - 办公用品管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deletePmpResOfficeSupplyByIds(String ids);

    /**
     * 删除资源管理 - 办公用品管理信息
     *
     * @param id 资源管理 - 办公用品管理ID
     * @return 结果
     */
    int deletePmpResOfficeSupplyById(Long id);
}

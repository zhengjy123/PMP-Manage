package com.ruoyi.web.app.resource.officesupply.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.stereotype.Service;
import com.ruoyi.web.app.resource.officesupply.mapper.PmpResOfficeSupplyMapper;
import com.ruoyi.web.app.resource.officesupply.domain.PmpResOfficeSupply;
import com.ruoyi.web.app.resource.officesupply.service.IPmpResOfficeSupplyService;
import com.ruoyi.common.core.text.Convert;

import javax.annotation.Resource;

/**
 * 资源管理 - 办公用品管理Service业务层处理
 *
 * @author ruoyi
 * @date 2024-07-20
 */
@Service
public class PmpResOfficeSupplyServiceImpl implements IPmpResOfficeSupplyService
{
    @Resource
    private PmpResOfficeSupplyMapper pmpResOfficeSupplyMapper;

    /**
     * 查询资源管理 - 办公用品管理
     *
     * @param id 资源管理 - 办公用品管理ID
     * @return 资源管理 - 办公用品管理
     */
    @Override
    public PmpResOfficeSupply selectPmpResOfficeSupplyById(Long id)
    {
        return pmpResOfficeSupplyMapper.selectPmpResOfficeSupplyById(id);
    }

    /**
     * 查询资源管理 - 办公用品管理列表
     *
     * @param pmpResOfficeSupply 资源管理 - 办公用品管理
     * @return 资源管理 - 办公用品管理
     */
    @Override
    public List<PmpResOfficeSupply> selectPmpResOfficeSupplyList(PmpResOfficeSupply pmpResOfficeSupply)
    {
        return pmpResOfficeSupplyMapper.selectPmpResOfficeSupplyList(pmpResOfficeSupply);
    }

    /**
     * 新增资源管理 - 办公用品管理
     *
     * @param pmpResOfficeSupply 资源管理 - 办公用品管理
     * @return 结果
     */
    @Override
    public int insertPmpResOfficeSupply(PmpResOfficeSupply pmpResOfficeSupply)
    {
        pmpResOfficeSupply.setCreateTime(DateUtils.getNowDate());
        return pmpResOfficeSupplyMapper.insertPmpResOfficeSupply(pmpResOfficeSupply);
    }

    /**
     * 修改资源管理 - 办公用品管理
     *
     * @param pmpResOfficeSupply 资源管理 - 办公用品管理
     * @return 结果
     */
    @Override
    public int updatePmpResOfficeSupply(PmpResOfficeSupply pmpResOfficeSupply)
    {
        return pmpResOfficeSupplyMapper.updatePmpResOfficeSupply(pmpResOfficeSupply);
    }

    /**
     * 删除资源管理 - 办公用品管理对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmpResOfficeSupplyByIds(String ids)
    {
        return pmpResOfficeSupplyMapper.deletePmpResOfficeSupplyByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除资源管理 - 办公用品管理信息
     *
     * @param id 资源管理 - 办公用品管理ID
     * @return 结果
     */
    @Override
    public int deletePmpResOfficeSupplyById(Long id)
    {
        return pmpResOfficeSupplyMapper.deletePmpResOfficeSupplyById(id);
    }
}

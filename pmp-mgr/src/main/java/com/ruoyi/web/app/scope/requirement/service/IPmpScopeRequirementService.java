package com.ruoyi.web.app.scope.requirement.service;

import com.ruoyi.web.app.scope.requirement.domain.PmpScopeRequirement;

import java.util.List;

/**
 * 范围管理-需求管理Service接口
 *
 * @author zjy
 * @date 2024-07-30
 */
public interface IPmpScopeRequirementService
{
    /**
     * 查询范围管理-需求管理
     *
     * @param id 范围管理-需求管理ID
     * @return 范围管理-需求管理
     */
    PmpScopeRequirement selectPmpScopeRequirementById(Long id);

    /**
     * 查询范围管理-需求管理列表
     *
     * @param pmpScopeRequirement 范围管理-需求管理
     * @return 范围管理-需求管理集合
     */
    List<PmpScopeRequirement> selectPmpScopeRequirementList(PmpScopeRequirement pmpScopeRequirement);

    /**
     * 新增范围管理-需求管理
     *
     * @param pmpScopeRequirement 范围管理-需求管理
     * @return 结果
     */
    int insertPmpScopeRequirement(PmpScopeRequirement pmpScopeRequirement);

    /**
     * 修改范围管理-需求管理
     *
     * @param pmpScopeRequirement 范围管理-需求管理
     * @return 结果
     */
    int updatePmpScopeRequirement(PmpScopeRequirement pmpScopeRequirement);

    /**
     * 批量删除范围管理-需求管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deletePmpScopeRequirementByIds(String ids);

    /**
     * 删除范围管理-需求管理信息
     *
     * @param id 范围管理-需求管理ID
     * @return 结果
     */
    int deletePmpScopeRequirementById(Long id);
}

package com.ruoyi.web.app.project.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.app.project.domain.PmpProjectInfo;
import com.ruoyi.web.app.project.service.IPmpProjectService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 项目信息管理Controller
 *
 * @author zjy
 * @date 2024-04-23
 */
@Controller
@RequestMapping("/system/project")
public class PmpProjectInfoController extends BaseController
{
    private String prefix = "app/project_info";

    @Resource
    private IPmpProjectService pmpProjectService;

    @RequiresPermissions("system:project:view")
    @GetMapping()
    public String project()
    {
        return prefix + "/list";
    }

    /**
     * 查询项目管理列表
     */
    @RequiresPermissions("system:project:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmpProjectInfo pmpProject)
    {
        startPage();
        List<PmpProjectInfo> list = pmpProjectService.selectPmpProjectList(pmpProject);
        return getDataTable(list);
    }

    /**
     * 导出项目管理列表
     */
    @RequiresPermissions("system:project:export")
    @Log(title = "项目管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmpProjectInfo pmpProject)
    {
        List<PmpProjectInfo> list = pmpProjectService.selectPmpProjectList(pmpProject);
        ExcelUtil<PmpProjectInfo> util = new ExcelUtil<>(PmpProjectInfo.class);
        return util.exportExcel(list, "project");
    }

    /**
     * 新增项目管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存项目管理
     */
    @RequiresPermissions("system:project:add")
    @Log(title = "项目管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmpProjectInfo pmpProject)
    {
        return toAjax(pmpProjectService.insertPmpProject(pmpProject));
    }

    /**
     * 修改项目管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PmpProjectInfo pmpProject = pmpProjectService.selectPmpProjectById(id);
        mmap.put("pmpProject", pmpProject);
        return prefix + "/edit";
    }

    /**
     * 修改保存项目管理
     */
    @RequiresPermissions("system:project:edit")
    @Log(title = "项目管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmpProjectInfo pmpProject)
    {
        return toAjax(pmpProjectService.updatePmpProject(pmpProject));
    }

    /**
     * 删除项目管理
     */
    @RequiresPermissions("system:project:remove")
    @Log(title = "项目管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pmpProjectService.deletePmpProjectByIds(ids));
    }
}

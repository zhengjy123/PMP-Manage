package com.ruoyi.web.app.resource.officesupply.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 资源管理 - 办公用品管理对象 pmp_res_office_supply
 *
 * @author ruoyi
 * @date 2024-07-20
 */
@Data
public class PmpResOfficeSupply extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 当前库存 */
    @Excel(name = "当前库存")
    private Long currentInventory;

    /** 单价 */
    @Excel(name = "单价")
    private Double price;

}

package com.ruoyi.web.app.resource.officesupply.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.web.app.resource.officesupply.domain.PmpResOfficeSupply;
import com.ruoyi.web.app.resource.officesupply.service.IPmpResOfficeSupplyService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.annotation.Resource;

/**
 * 资源管理 - 办公用品管理Controller
 *
 * @author ruoyi
 * @date 2024-07-20
 */
@Controller
@RequestMapping("/system/supply")
public class PmpResOfficeSupplyController extends BaseController
{
    private String prefix = "app/res_office";

    @Resource
    private IPmpResOfficeSupplyService pmpResOfficeSupplyService;

    @RequiresPermissions("system:supply:view")
    @GetMapping()
    public String supply()
    {
        return prefix + "/list";
    }

    /**
     * 查询资源管理 - 办公用品管理列表
     */
    @RequiresPermissions("system:supply:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmpResOfficeSupply pmpResOfficeSupply)
    {
        startPage();
        List<PmpResOfficeSupply> list = pmpResOfficeSupplyService.selectPmpResOfficeSupplyList(pmpResOfficeSupply);
        return getDataTable(list);
    }

    /**
     * 导出资源管理 - 办公用品管理列表
     */
    @RequiresPermissions("system:supply:export")
    @Log(title = "资源管理 - 办公用品管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmpResOfficeSupply pmpResOfficeSupply)
    {
        List<PmpResOfficeSupply> list = pmpResOfficeSupplyService.selectPmpResOfficeSupplyList(pmpResOfficeSupply);
        ExcelUtil<PmpResOfficeSupply> util = new ExcelUtil<PmpResOfficeSupply>(PmpResOfficeSupply.class);
        return util.exportExcel(list, "supply");
    }

    /**
     * 新增资源管理 - 办公用品管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存资源管理 - 办公用品管理
     */
    @RequiresPermissions("system:supply:add")
    @Log(title = "资源管理 - 办公用品管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmpResOfficeSupply pmpResOfficeSupply)
    {
        return toAjax(pmpResOfficeSupplyService.insertPmpResOfficeSupply(pmpResOfficeSupply));
    }

    /**
     * 修改资源管理 - 办公用品管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PmpResOfficeSupply pmpResOfficeSupply = pmpResOfficeSupplyService.selectPmpResOfficeSupplyById(id);
        mmap.put("pmpResOfficeSupply", pmpResOfficeSupply);
        return prefix + "/edit";
    }

    /**
     * 修改保存资源管理 - 办公用品管理
     */
    @RequiresPermissions("system:supply:edit")
    @Log(title = "资源管理 - 办公用品管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmpResOfficeSupply pmpResOfficeSupply)
    {
        return toAjax(pmpResOfficeSupplyService.updatePmpResOfficeSupply(pmpResOfficeSupply));
    }

    /**
     * 删除资源管理 - 办公用品管理
     */
    @RequiresPermissions("system:supply:remove")
    @Log(title = "资源管理 - 办公用品管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pmpResOfficeSupplyService.deletePmpResOfficeSupplyByIds(ids));
    }
}

package com.ruoyi.web.app.schedule.module.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.web.app.schedule.module.domain.PmpScheduleModule;
import com.ruoyi.web.app.schedule.module.mapper.PmpScheduleModuleMapper;
import com.ruoyi.web.app.schedule.module.service.IPmpScheduleModuleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 进度-模块管理Service业务层处理
 *
 * @author zjy
 * @date 2024-08-25
 */
@Service
public class PmpScheduleModuleServiceImpl implements IPmpScheduleModuleService
{
    @Resource
    private PmpScheduleModuleMapper pmpScheduleModuleMapper;

    /**
     * 查询进度-模块管理
     *
     * @param id 进度-模块管理ID
     * @return 进度-模块管理
     */
    @Override
    public PmpScheduleModule selectPmpScheduleModuleById(Long id)
    {
        return pmpScheduleModuleMapper.selectPmpScheduleModuleById(id);
    }

    /**
     * 查询进度-模块管理列表
     *
     * @param pmpScheduleModule 进度-模块管理
     * @return 进度-模块管理
     */
    @Override
    public List<PmpScheduleModule> selectPmpScheduleModuleList(PmpScheduleModule pmpScheduleModule)
    {
        return pmpScheduleModuleMapper.selectPmpScheduleModuleList(pmpScheduleModule);
    }

    /**
     * 新增进度-模块管理
     *
     * @param pmpScheduleModule 进度-模块管理
     * @return 结果
     */
    @Override
    public int insertPmpScheduleModule(PmpScheduleModule pmpScheduleModule)
    {
        pmpScheduleModule.setCreateTime(DateUtils.getNowDate());
        return pmpScheduleModuleMapper.insertPmpScheduleModule(pmpScheduleModule);
    }

    /**
     * 修改进度-模块管理
     *
     * @param pmpScheduleModule 进度-模块管理
     * @return 结果
     */
    @Override
    public int updatePmpScheduleModule(PmpScheduleModule pmpScheduleModule)
    {
        return pmpScheduleModuleMapper.updatePmpScheduleModule(pmpScheduleModule);
    }

    /**
     * 删除进度-模块管理对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmpScheduleModuleByIds(String ids)
    {
        return pmpScheduleModuleMapper.deletePmpScheduleModuleByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除进度-模块管理信息
     *
     * @param id 进度-模块管理ID
     * @return 结果
     */
    @Override
    public int deletePmpScheduleModuleById(Long id)
    {
        return pmpScheduleModuleMapper.deletePmpScheduleModuleById(id);
    }
}

package com.ruoyi.web.app.cost.service;


import com.ruoyi.web.app.project.domain.PmpProjectInfo;

import java.util.List;

/**
 * 项目成本管理Service接口
 *
 * @author zjy
 * @date 2024-04-23
 */
public interface IPmpProjectCostService
{
    /**
     * 查询项目管理
     *
     * @param id 项目管理ID
     * @return 项目管理
     */
    PmpProjectInfo selectPmpProjectById(Long id);

    /**
     * 查询项目管理列表
     *
     * @param pmpProject 项目管理
     * @return 项目管理集合
     */
    List<PmpProjectInfo> selectPmpProjectList(PmpProjectInfo pmpProject);

}

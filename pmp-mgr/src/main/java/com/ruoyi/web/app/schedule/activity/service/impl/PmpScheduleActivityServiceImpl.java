package com.ruoyi.web.app.schedule.activity.service.impl;

import java.util.List;

import com.ruoyi.web.app.schedule.activity.domain.PmpScheduleActivity;
import com.ruoyi.web.app.schedule.activity.mapper.PmpScheduleActivityMapper;
import com.ruoyi.web.app.schedule.activity.service.IPmpScheduleActivityService;
import org.springframework.stereotype.Service;

import com.ruoyi.common.core.text.Convert;

import javax.annotation.Resource;

/**
 * 项目进度管理-活动清单Service业务层处理
 *
 * @author zjy
 * @date 2024-07-18
 */
@Service
public class PmpScheduleActivityServiceImpl implements IPmpScheduleActivityService
{
    @Resource
    private PmpScheduleActivityMapper pmpScheduleActivityMapper;

    /**
     * 查询项目进度管理-活动清单
     *
     * @param id 项目进度管理-活动清单ID
     * @return 项目进度管理-活动清单
     */
    @Override
    public PmpScheduleActivity selectPmpScheduleActivityById(Long id)
    {
        return pmpScheduleActivityMapper.selectPmpScheduleActivityById(id);
    }

    /**
     * 查询项目进度管理-活动清单列表
     *
     * @param pmpScheduleActivity 项目进度管理-活动清单
     * @return 项目进度管理-活动清单
     */
    @Override
    public List<PmpScheduleActivity> selectPmpScheduleActivityList(PmpScheduleActivity pmpScheduleActivity)
    {
        return pmpScheduleActivityMapper.selectPmpScheduleActivityList(pmpScheduleActivity);
    }

    /**
     * 新增项目进度管理-活动清单
     *
     * @param pmpScheduleActivity 项目进度管理-活动清单
     * @return 结果
     */
    @Override
    public int insertPmpScheduleActivity(PmpScheduleActivity pmpScheduleActivity)
    {
        return pmpScheduleActivityMapper.insertPmpScheduleActivity(pmpScheduleActivity);
    }

    /**
     * 修改项目进度管理-活动清单
     *
     * @param pmpScheduleActivity 项目进度管理-活动清单
     * @return 结果
     */
    @Override
    public int updatePmpScheduleActivity(PmpScheduleActivity pmpScheduleActivity)
    {
        return pmpScheduleActivityMapper.updatePmpScheduleActivity(pmpScheduleActivity);
    }

    /**
     * 删除项目进度管理-活动清单对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmpScheduleActivityByIds(String ids)
    {
        return pmpScheduleActivityMapper.deletePmpScheduleActivityByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除项目进度管理-活动清单信息
     *
     * @param id 项目进度管理-活动清单ID
     * @return 结果
     */
    @Override
    public int deletePmpScheduleActivityById(Long id)
    {
        return pmpScheduleActivityMapper.deletePmpScheduleActivityById(id);
    }
}

package com.ruoyi.web.app.schedule.activity.controller;

import java.util.List;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.app.schedule.activity.domain.PmpScheduleActivity;
import com.ruoyi.web.app.schedule.activity.service.IPmpScheduleActivityService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.annotation.Resource;

/**
 * 项目进度管理-活动清单Controller
 *
 * @author zjy
 * @date 2024-07-18
 */
@Controller
@RequestMapping("/system/activity")
public class PmpScheduleActivityController extends BaseController
{
    private String prefix = "app/activity";

    @Resource
    private IPmpScheduleActivityService pmpScheduleActivityService;

    @RequiresPermissions("system:activity:view")
    @GetMapping()
    public String activity()
    {
        return prefix + "/activity";
    }

    /**
     * 查询项目进度管理-活动清单列表
     */
    @RequiresPermissions("system:activity:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmpScheduleActivity pmpScheduleActivity)
    {
        startPage();
        List<PmpScheduleActivity> list = pmpScheduleActivityService.selectPmpScheduleActivityList(pmpScheduleActivity);
        return getDataTable(list);
    }

    /**
     * 导出项目进度管理-活动清单列表
     */
    @RequiresPermissions("system:activity:export")
    @Log(title = "项目进度管理-活动清单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmpScheduleActivity pmpScheduleActivity)
    {
        List<PmpScheduleActivity> list = pmpScheduleActivityService.selectPmpScheduleActivityList(pmpScheduleActivity);
        ExcelUtil<PmpScheduleActivity> util = new ExcelUtil<PmpScheduleActivity>(PmpScheduleActivity.class);
        return util.exportExcel(list, "activity");
    }

    /**
     * 新增项目进度管理-活动清单
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存项目进度管理-活动清单
     */
    @RequiresPermissions("system:activity:add")
    @Log(title = "项目进度管理-活动清单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmpScheduleActivity pmpScheduleActivity)
    {
        return toAjax(pmpScheduleActivityService.insertPmpScheduleActivity(pmpScheduleActivity));
    }

    /**
     * 修改项目进度管理-活动清单
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PmpScheduleActivity pmpScheduleActivity = pmpScheduleActivityService.selectPmpScheduleActivityById(id);
        mmap.put("pmpScheduleActivity", pmpScheduleActivity);
        return prefix + "/edit";
    }

    /**
     * 修改保存项目进度管理-活动清单
     */
    @RequiresPermissions("system:activity:edit")
    @Log(title = "项目进度管理-活动清单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmpScheduleActivity pmpScheduleActivity)
    {
        return toAjax(pmpScheduleActivityService.updatePmpScheduleActivity(pmpScheduleActivity));
    }

    /**
     * 删除项目进度管理-活动清单
     */
    @RequiresPermissions("system:activity:remove")
    @Log(title = "项目进度管理-活动清单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pmpScheduleActivityService.deletePmpScheduleActivityByIds(ids));
    }
}

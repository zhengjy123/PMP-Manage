package com.ruoyi.web.app.project.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.web.app.project.domain.PmpProjectInfo;
import com.ruoyi.web.app.project.mapper.PmpProjectInfoMapper;
import com.ruoyi.web.app.project.service.IPmpProjectService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 项目管理Service业务层处理
 *
 * @author zjy
 * @date 2024-04-23
 */
@Service
public class PmpProjectServiceImpl implements IPmpProjectService
{
    @Resource
    private PmpProjectInfoMapper pmpProjectMapper;

    /**
     * 查询项目管理
     *
     * @param id 项目管理ID
     * @return 项目管理
     */
    @Override
    public PmpProjectInfo selectPmpProjectById(Long id)
    {
        return pmpProjectMapper.selectPmpProjectInfoById(id);
    }

    /**
     * 查询项目管理列表
     *
     * @param pmpProject 项目管理
     * @return 项目管理
     */
    @Override
    public List<PmpProjectInfo> selectPmpProjectList(PmpProjectInfo pmpProject)
    {
        return pmpProjectMapper.selectPmpProjectInfoList(pmpProject);
    }

    /**
     * 新增项目管理
     *
     * @param pmpProject 项目管理
     * @return 结果
     */
    @Override
    public int insertPmpProject(PmpProjectInfo pmpProject)
    {
        // 需要判断项目名称是否重复
        if (pmpProjectMapper.isProjectNameExists(pmpProject.getName())){
            throw new BusinessException("项目名称重复，请修改");
        }

        // 获取当前的用户
        SysUser currentUser = ShiroUtils.getSysUser();

        pmpProject.setCreatedTime(DateUtils.getNowDate());
        pmpProject.setCreatedBy(currentUser.getUserName());

        return pmpProjectMapper.insertPmpProjectInfo(pmpProject);
    }

    /**
     * 修改项目管理
     *
     * @param pmpProject 项目管理
     * @return 结果
     */
    @Override
    public int updatePmpProject(PmpProjectInfo pmpProject)
    {
        return pmpProjectMapper.updatePmpProjectInfo(pmpProject);
    }

    /**
     * 删除项目管理对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmpProjectByIds(String ids)
    {
        return pmpProjectMapper.deletePmpProjectInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除项目管理信息
     *
     * @param id 项目管理ID
     * @return 结果
     */
    @Override
    public int deletePmpProjectById(Long id)
    {
        return pmpProjectMapper.deletePmpProjectInfoById(id);
    }
}

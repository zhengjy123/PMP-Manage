package com.ruoyi.web.app.schedule.module.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 进度-模块管理对象 pmp_schedule_module
 *
 * @author zjy
 * @date 2024-08-25
 */
@Data
public class PmpScheduleModule extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 模块名称 */
    @Excel(name = "模块名称")
    private String name;

    /** 模块描述 */
    @Excel(name = "模块描述")
    private String decription;

}

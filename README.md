
## PMP项目管理系统(研发中)
根据pmp思想独立设计的项目管理系统

## 文档
[【腾讯文档】数据库设计](https://docs.qq.com/sheet/DYUR4b0p0Q3JhVmNs?tab=746ho2)

[在线论坛](http://42.194.151.54)

## 十五至尊
![输入图片说明](https://img1.baidu.com/it/u=778857942,2880974647&fm=253&fmt=auto&app=138&f=PNG?w=795&h=500)

## 1.3.0已开发的主要功能
### 项目管理
- 整合管理
    1. 项目信息管理
    2. 项目人员管理
- 范围管理
- 进度管理
    1. 活动管理
- 成本管理
- 资源管理
    1. 办公用品管理
- 风险管理
    1.  风险登记册


## 作者
QQ: 394341810

## 在线体验

演示地址：http://42.194.151.54:8082/

> 管理账号：admin / admin
>
> 请假流程测试账号
>
> 普通员工：chengxy / 123456
>
> 部门领导：axianlu / 123456
>
> 人事：rensm / 123456
>
## 项目使用框架

官方文档：[闲鹿技术](http://doc.xianlutech.com)

[前后端分离版：http://106.12.122.249/](http://106.12.122.249/)

>  闲鹿工作流是一款基于 RuoYi 4.x + Activiti 6.x + Spring Boot 2.x + Thymeleaf 3.x 的开源工作流管理系统~
>
>  作为技术小菜鸟的我，一直对学习 Activiti 工作流框架求之不得，断断续续入门入了三次。这次能够写出这个项目主要归功于 ☕🐇 的[《Activiti 实战》](https://github.com/henryyan/activiti-in-action-codes)。这本书给予了我很大的帮助。最后但仍然重要的，我要感谢[若依框架](http://www.ruoyi.vip/)，她让我实现快速集成工作流 WEB 应用。—— 一只闲鹿
>
>  参考资料👇
>
>  1. 若依框架: [http://www.ruoyi.vip](http://www.ruoyi.vip/)
>  2. 咖啡兔：[《Activiti 实战》](https://github.com/henryyan/activiti-in-action-codes)
>  3. Activiti User Guide: <https://www.activiti.org/userguide/index.html#springSpringBoot>
>  4. XBoot: [http://xboot.exrick.cn](http://xboot.exrick.cn/)

## 内置功能

v1.0
1. Activiti Modeler 完全汉化。


## 演示图

<table>
    <tr>
        <td><img src="screenshot/main.png"/></td>
        <td><img src="screenshot/online.png"/></td>
    </tr>
    <tr>
        <td><img src="screenshot/define.png"/></td>
        <td><img src="screenshot/user.png"/></td>
    </tr>
    <tr>
        <td><img src="screenshot/useradd.png"/></td>
        <td><img src="screenshot/group.png"/></td>
    </tr>
    <tr>
        <td><img src="screenshot/groupadd.png"/></td>
        <td><img src="screenshot/list.png"/></td>
    </tr>
    <tr>
        <td><img src="screenshot/add.png"/></td>
        <td><img src="screenshot/detail.png"/></td>
    </tr>
    <tr>
        <td><img src="screenshot/history.png"/></td>
        <td><img src="screenshot/process.png"/></td>
    </tr>
    <tr>
        <td><img src="screenshot/todo.png"/></td>
        <td><img src="screenshot/done.png"/></td>
    </tr>
    <tr>
        <td><img src="screenshot/process2.png"/></td>
        <td><img src="screenshot/select.png"/></td>
    </tr>
    <tr>
        <td><img src="screenshot/my-todo.png"/></td>
        <td><img src="screenshot/handle.png"/></td>
    </tr>
    <tr>
        <td><img src="screenshot/todoitem.png"/></td>
        <td><img src="screenshot/doneitem.png"/></td>
    </tr>
    <tr>
        <td><img src="screenshot/modellist.png"/></td>
        <td><img src="screenshot/modeler.png"/></td>
    </tr>
</table>

## 闲鹿工作流交流群

QQ群：794711759(满)、813539310（满）、1137171616（满）、767590879。

![闲鹿工作流交流群](screenshot/qqgroup.png)
![闲鹿工作流交流群2](screenshot/qqgroup2.png)

## 视频教程

[【闲鹿课堂】2020最新 Activiti6整合Spring Boot2快速入门教程](<https://www.bilibili.com/video/BV1Fp4y19729>)



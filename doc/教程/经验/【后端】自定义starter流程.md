



# 如何在项目中自定义starter

## 1.在idea中新建java模块

<img src="assets/2024-08-28_191057.png" alt="2024-08-28_191057" style="zoom:67%;" /> 

## 2.编辑pom文件，引用父模块和spring一些基础模块

案例

```java
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>ruoyi</artifactId>
        <groupId>com.ruoyi</groupId>
        <version>4.2.0</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.ruoyi</groupId>
    <artifactId>hello-spring-starter</artifactId>

   <description>
       common通用工具
   </description>

    <dependencies>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>

        <!--解决idea对配置类的警告问题-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>
    </dependencies>

</project>
```

## 3.编写业务类

```java
@Data
public class CustomService {

    CustomProperties properties;

    public String sayHello(String name) {
        return properties.getPrefix()+"hello"+name;
    }
}
```

## 4.编写属性类

```
@ConfigurationProperties(prefix = "custom")
@Data
public class CustomProperties {

    private String prefix;
}
```

## 5.编写自动备注类，程序将会将类自动转载到容器中以供使用

```
@Configuration
// 让属性文件生效
@EnableConfigurationProperties(CustomProperties.class)
public class CustomConfiguration {

    @Autowired
    CustomProperties customProperties;

    @Bean
    public CustomService customService() {
        CustomService customService = new CustomService();
        customService.setProperties(customProperties);
        return customService;
    }
}
```

## 6.在src/resources目录下，创建META-INF,创建spring.factories，在里面定义自动装配的类

```
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
    com.zjy.config.CustomConfiguration
    // 如果需要多个在后面添加
    // org.activiti.spring.boot.EndpointAutoConfiguration,\ 
```

## 7.在需要mainApp包含的pom文件中，加入对改启动器的maven坐标

```java
<!--自定义starter demo-->
<dependency>
    <groupId>com.ruoyi</groupId>
    <artifactId>hello-spring-starter</artifactId>
    <version>4.2.0</version>
</dependency>
```

## 8.在mainApp的application.yml文件中，加入以下配置

```xml
# 自定义starter测试
custom:
  prefix: ppp
```

## 9.编写单元测试

```java
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PMPManageApplication.class)
@Slf4j
public class MemberTest {

    // 自定义starter 测试
    @Resource
    CustomService customerService;

    /**
     * 自定义starter
     */
    @Test
    public void customStarterTest() {
        String hello = customerService.sayHello("123");
        log.debug("{}", hello);
    }
}
```

## 10.测试效果

![2024-08-28_193823](assets/2024-08-28_193823.png)

视频教程：

https://www.bilibili.com/video/BV1DZ4y1p7W9?p=70&vd_source=6d981861200e8e4c9ef45dbffb0cfd11

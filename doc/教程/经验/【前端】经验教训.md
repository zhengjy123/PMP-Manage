

## 常用需求

### 表格操作





## 问题日志

### 2024-07-20

#### 问题01: jQuery3.0+报错Uncaught TypeError: e.indexOf is not a function

**现象**


```text
jQuery3.0+报错Uncaught TypeError: e.indexOf is not a function
使用.load()绑定事件时报错，Uncaught TypeError: e.indexOf is not a function
显示出错的代码为$('#id').load(function () {}
因为 .load() 在 jQuery1.8 时不建议使用，在 jQuery3.0 时弃用
使用 $('#id').on('load', function() {}) 代替即可
 
同时在3.0+被弃用的还有：
.error() ：使用.on( "error", handle)代替
.unload() ：使用.on( "unload", handle)代替
在3.0+不建议使用的有：
.bind()、.unbind()、.delegate()、.undelegate()
使用 .on() 和 .off() 代替
```

 **来源事件** ：进入系统，点击选项卡JS会报错，排查后发现以下代码有毒
```html
    // 有毒代码 使用过时的方法
    // $('.mainContent iframe:visible', topWindow).load(function () {
        // 	window.parent.$.modal.closeLoading();
        // });

    
```

**解决方案:**

```
	// 修改后
    $('.mainContent iframe:visible', topWindow).on('load', function() {
		window.parent.$.modal.closeLoading();
    });
```

#### 2024-08-25

#### 问题: 执行表格插入后，数据加载遮罩层一直处于loading状态，但是刷新后发现数据又正常插入

**现象**

图1-点击提交，无限加载

![2024-08-25_114315](assets/2024-08-25_114315.png) 

图2-刷新浏览器，发现数据已经被添加

![2024-08-25_114454](assets/2024-08-25_114454.png) 

**排查思路历程**：

1.确定是后端代码bug还是前端代码bug

2.通过浏览器的F12，打开调试器，打开网络，发现网络请求(/add)有正常返回，再结合图2说明，数据是有被正常插入的，那么后端出bug问题概率可能不大，但是还是要考虑后端是否还有做额外的IO操作，经过排查发现，后端会记录用户的操作日志。这里也有涉及到数据库的IO，但是排查和回忆后，感觉也没有什么太大问题，因此我将问题的重点放到了前端脚本上。

3.排查前端，首先还是通过浏览器F12调试工具，打开调试器，果然，发现了报错日志，日志显示，某一个变量找不到

![2024-08-25_115159](assets/2024-08-25_115159.png) 

4.根据日志提示信息，定位追踪到ry-ui.js,这是若依对表跟进行通用操作封装的代码，然后根据堆栈信息，定位到具体位置![2024-08-25_115434](assets/2024-08-25_115434.png) 

5.经过研究，感觉这段代码本身没什么问题，而且原先也没有改动，也是正常，那么为什么table找不到呢，于是继续追踪table的定义，终于，发现了端倪 

![2024-08-25_115657](assets/2024-08-25_115657.png)

6.原来，之前由于IDEA默认遵循ES6+标准(而框架作者编码的环境是基于ES5，所以不会报这个警告)，会对var变量做警告处理，于是我为了消除IDE警告，在没有仔细对框架代码做全局影响评估的情况下，将var改成了let，然后也没做测试(以为这种小修改不会造成什么影响)，不经意间产生了这段有毒代码，所以导致产生了这个问题。

**解决方案**

1. 先追踪git日志，查看修改的时间点，然后做回滚处理

2. 重新启动，发现问题已解决

3. 找到IDEA设置，将ES6标准退回ES5，避免编译器警告 

   ![2024-08-25_120717](assets/2024-08-25_120717.png)

4. 删除调式代码，重新提交代码

5. 文档中记录经验教训


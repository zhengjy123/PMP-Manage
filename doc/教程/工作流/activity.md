https://www.yisu.com/jc/566899.html

## activity 表说明
1 act_ge_bytearray 二进制数据表

2 act_ge_property 属性数据表存储整个流程引擎级别的数据,初始化表结构时，会默认插入三条记录，

3 act_hi_actinst 历史节点表

4 act_hi_attachment 历史附件表

5 act_hi_comment 历史意见表

6 act_hi_identitylink 历史流程人员表

7 act_hi_detail 历史详情表，提供历史变量的查询

8 act_hi_procinst 历史流程实例表

9 act_hi_taskinst 历史任务实例表

10 act_hi_varinst 历史变量表

11 act_id_group 用户组信息表

12 act_id_info 用户扩展信息表

13 act_id_membership 用户与用户组对应信息表

14 act_id_user 用户信息表

15 act_re_deployment 部署信息表

16 act_re_model 流程设计模型部署表

17 act_re_procdef 流程定义数据表

18 act_ru_event_subscr throwEvent、catchEvent时间监听信息表

19 act_ru_execution 运行时流程执行实例表

20 act_ru_identitylink 运行时流程人员表，主要存储任务节点与参与者的相关信息

21 act_ru_job 运行时定时任务数据表

22 act_ru_task 运行时任务节点表

23 act_ru_variable 运行时流程变量数据表

## activity 常见活动类型
在Activiti中，常见的活动类型包括但不限于：

    userTask：用户任务。这是需要人工干预的任务，通常会在BPMN图中用一个带有“用户”图标的矩形表示。
    serviceTask：服务任务。这是一个自动化任务，可以由Java Delegate、表达式、Camel路由等多种方式实现。在BPMN图中，它通常用一个普通的矩形表示。
    scriptTask：脚本任务。这也是一个自动化任务，但它执行的是脚本代码。在BPMN图中，它可能用一个带有“脚本”图标的矩形表示，但通常与serviceTask在视觉上难以区分。
    sendTask：发送任务。这通常与消息发送相关，用于在流程中发送消息给外部系统或组件。
    receiveTask：接收任务。与发送任务相对，这是等待接收消息的任务。
    startEvent：开始事件。表示流程的开始点，在BPMN图中用圆圈表示。
    endEvent：结束事件。表示流程的结束点，也在BPMN图中用圆圈表示，但通常与开始事件的圆圈有所不同（例如，带有“X”标记）。
    gateway（如exclusiveGateway, parallelGateway, inclusiveGateway等）：网关。网关用于控制流程的分叉和汇合。常见的网关有排他网关（XOR）、并行网关（AND）、包容网关（OR）等。
    subProcess：子流程。子流程是嵌套在另一个流程中的流程实例，它可以是任意复杂的流程。
    callActivity：调用活动。这是一种特殊的活动，用于调用另一个流程定义作为当前流程的一部分来执行。


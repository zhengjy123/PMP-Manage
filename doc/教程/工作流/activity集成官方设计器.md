

https://www.bilibili.com/video/BV1Fp4y19729?p=22&vd_source=6d981861200e8e4c9ef45dbffb0cfd11

# 集成ActivityModeler

1.集成前端代码

![2024-08-20_235641](assets/2024-08-20_235641.png) 

修改app-cfg.js

```
ACTIVITI.CONFIG = {
   'contextRoot' : '/modeler',
};
```

2.前端根目录 stencilset.json 编辑器汉化

i18n下 en.json 汉化

toolbar-default-action.js 将PUT 改成POST

![2024-08-21_010451](assets/2024-08-21_010451.png) 

3.后端整合

![2024-08-21_010633](assets/2024-08-21_010633.png) 

4.需要修改路径，前面加/modeler (4个类都需要修改)

```
@RequestMapping(value="/modeler/editor/stencilset", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
```

5.ShiroConfig 添加 匿名访问

启动容器，访问 /modeler/modeler.html 




## 简单整合redis




## maven依赖

pom.xml

```html
		
		<dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-pool2</artifactId>
            <version>2.6.2</version>
        </dependency>

        <!-- springboot整合redis -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-redis</artifactId>
        </dependency>

```
## 基础配置

```java
@Configuration
public class RedisConfig {
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
        //创建RedisTemplate对象
        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        //设置连接工厂
        template.setConnectionFactory(connectionFactory);
        //创建json序列化工具
        GenericJackson2JsonRedisSerializer jsonRedisSerializer = new GenericJackson2JsonRedisSerializer();
        //设置key的序列化
        template.setKeySerializer(RedisSerializer.string());
        template.setHashKeySerializer(RedisSerializer.string());
        //设置value的序列化
        template.setValueSerializer(jsonRedisSerializer);
        template.setHashValueSerializer(jsonRedisSerializer);
        //返回
        return template;
    }
}

```

## 应用

```java

@RestController
@RequestMapping("test/redis")
public class RedisUtil {

    @Resource
    private RedisTemplate redisTemplate;

    @GetMapping("save")
    public AjaxResult save(String key, String value){
        redisTemplate.opsForValue().set(key, value);
        AjaxResult result = new AjaxResult();
        result.put("message","value ["+value+"] is save to redis");
        return result;
    }

    @GetMapping("get")
    public AjaxResult save(String key){
        Object value = redisTemplate.opsForValue().get(key);
        AjaxResult result = new AjaxResult();
        result.put("message","value is ["+value+"]");
        return result;
    }

}
```


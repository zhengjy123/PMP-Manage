## shiro整合redis

**背景介绍：**

为什么要将ehache改成redis？

由于当前使用的若依框架默认使用ehcache(即JVM内存)做为缓存策略，如果是单机部署是合适的，但如果随着访问量增大，服务结点用2个以上时，那么如果nginx不使用(ip-hash做为负载策略)就会出现一个问题，假设用户在节点A登录，会话信息保存在容器A的内存中，如果用户的下一次访问负载到了节点B，由于节点B没有存储用户的会话信息，那么用户需要重新登陆，会话无法保证一致性。

官方教程 http://doc.ruoyi.vip/ruoyi/document/cjjc.html#%E9%9B%86%E6%88%90redis%E5%AE%9E%E7%8E%B0%E9%9B%86%E7%BE%A4%E4%BC%9A%E8%AF%9D

由于信息不完整，故出了自己的教程

## springboot整合redis-略

参考另一份教程 [框架整合redis.md](框架整合redis.md)  



## shiro整合redis具体步骤

ruoyi-framework 下的pom 添加以下依赖

```xml
		<!-- shiro整合redis Shiro-redis插件-->
		<dependency>
			<groupId>org.crazycake</groupId>
			<artifactId>shiro-redis</artifactId>
			<version>3.3.1</version>
			<exclusions>
				<exclusion>
					<groupId>org.apache.velocity</groupId>
					<artifactId>velocity</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
```

修改ShiroConfig, 添加两个bean

```java
/**
     * 配置Redis管理器
     * 这里的RedisManager使用的是shiro-redis开源插件
     */
    @Bean
    public RedisManager redisManager() {
        RedisManager redisManager = new RedisManager();
        redisManager.setHost("127.0.0.1:6379");
        redisManager.setTimeout(3000);
        // 我的redis没有设置密码
//        redisManager.setPassword(password);
        return redisManager;
    }

    /**
     * 配置Cache管理器
     * 缓存控制器，来管理如用户、角色、权限等的缓存的
     * 用于往Redis存储权限和角色标识
     * 这里的RedisCacheManager使用的是shiro-redis开源插件
     */
    @Bean
    public RedisCacheManager redisCacheManager() {
        RedisCacheManager redisCacheManager = new RedisCacheManager();
        redisCacheManager.setRedisManager(redisManager());
        redisCacheManager.setKeyPrefix("shiro::");
        // 配置缓存的话要求放在session里面的实体类必须有个id标识
        redisCacheManager.setPrincipalIdFieldName("userId");
        return redisCacheManager;
    }
```

删除或注释 getEhCacheManager，然后将相关的方法改成redisCacheManager

启动服务，打开rdm客户端，验证会话是否成功保存 

![2024-08-25_230148](assets/2024-08-25_230148.png)

这里给出一份参考示例ShiroConfig

 [ShiroRedisConfig.java](assets\ShiroRedisConfig.java) 

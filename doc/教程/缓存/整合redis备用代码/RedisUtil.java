package com.ruoyi.common.config;

import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("test/redis")
public class RedisUtil {

    @Resource
    private RedisTemplate redisTemplate;

    @GetMapping("save")
    public AjaxResult save(String key, String value){
        redisTemplate.opsForValue().set(key, value);
        AjaxResult result = new AjaxResult();
        result.put("message","value ["+value+"] is save to redis");
        return result;
    }

    @GetMapping("get")
    public AjaxResult save(String key){
        Object value = redisTemplate.opsForValue().get(key);
        AjaxResult result = new AjaxResult();
        result.put("message","value is ["+value+"]");
        return result;
    }

}

package com.zjy.service;


import com.zjy.config.CustomProperties;
import lombok.Data;

/**
 * @Description:
 * @Author: zjy
 * @Date: 2024/8/23 19:36
 */
@Data
public class CustomService {

    CustomProperties properties;

    public String sayHello(String name) {
        return properties.getPrefix()+"hello:"+name;
    }
}

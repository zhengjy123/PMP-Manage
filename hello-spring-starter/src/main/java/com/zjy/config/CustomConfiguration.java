package com.zjy.config;

import com.zjy.service.CustomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Author: zjy
 * @Date: 2024/8/23 19:47
 */
@Configuration
// 让属性文件生效
@EnableConfigurationProperties(CustomProperties.class)
public class CustomConfiguration {

    @Autowired
    CustomProperties customProperties;

    @Bean
    public CustomService customService() {
        CustomService customService = new CustomService();
        customService.setProperties(customProperties);
        return customService;
    }
}

package com.zjy.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Description:
 * @Author: zjy
 * @Date: 2024/8/23 19:47
 */
@ConfigurationProperties(prefix = "custom")
@Data
public class CustomProperties {

    private String prefix;
}

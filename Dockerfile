# 使用官方的OpenJDK 8作为基础镜像  
FROM openjdk:8-jdk-alpine  
  
# 设置工作目录为/app  
WORKDIR /app  
  
# 将当前目录下的app.jar复制到容器的/app目录下  
COPY ./pmp-mgr.jar /app/pmp-mgr.jar
# 拷贝配置文件
COPY ./application.yml 		/app
COPY ./application-test.yml /app
COPY ./ry.sh                /app
  
# 暴露端口  
EXPOSE 8082  
  
# 设置容器启动时运行的命令  
ENTRYPOINT ["java","-jar","/app/pmp-mgr.jar"]